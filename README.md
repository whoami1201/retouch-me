## How to run

### Installing dependencies
`npm install`

## Build

Running bundle

`react-native start --reset-cache`

Run on iOS

`react-native run-ios`

Run on Android

`react-native run-android`
