import CryptoJS from 'crypto-js'
import { CLOUDINARY } from '../config'

export function uploadImageCloudinary(uri) {
  const {
    API_KEY,
    CLOUD_NAME,
    API_URL,
    API_SECRET,
  } = CLOUDINARY
  const timestamp = (Date.now() / 1000 || 0).toString()
  const hashString = `timestamp=${timestamp}${API_SECRET}`
  const signature = CryptoJS.SHA1(hashString).toString()
  const UPLOAD_URL = `${API_URL}${CLOUD_NAME}/image/upload`

  const formData = new FormData()
  formData.append('file', { uri, type: 'image/jpg', name: timestamp })
  formData.append('timestamp', timestamp)
  formData.append('api_key', API_KEY)
  formData.append('signature', signature)

  const config = {
    method: 'POST',
    body: formData,
  }

  return fetch(UPLOAD_URL, config)
    .then((res) => res.json())
    .then((response) => response)
    .catch((err) => err)
}

export const DEFAULT_RESIZE_METHOD = 'w_300,h_300,c_fill'
export const RESIZE_W_800 = 'w_800'

export const resizePhotoUrl = (url, method = DEFAULT_RESIZE_METHOD) => (
  url.replace('/upload', `/upload/${method}`)
)
