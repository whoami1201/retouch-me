import React from 'react'
import {
  Image,
  TouchableHighlight,
  StyleSheet,
} from 'react-native'

import colors from '../../../constants/colors'
import dimensions from '../../../constants/dimensions'
import CameraIcon from '../../../assets/icons/camera.png'

const CameraButton = (props) => (
  <TouchableHighlight
    underlayColor={colors.transparent}
    onPress={props.onPress}
  >
    <Image
      style={styles.camera}
      source={CameraIcon}
    />
  </TouchableHighlight>
)

const styles = StyleSheet.create({
  camera: {
    marginLeft: dimensions.margin,
  },
})

export default CameraButton
