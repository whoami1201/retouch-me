import React from 'react'
import {
  View,
  Text,
  TouchableOpacity,
  StyleSheet,
  Image,
} from 'react-native'

import colors from '../../../constants/colors'
import dimensions from '../../../constants/dimensions'
import fonts from '../../../constants/fonts'

const PrimaryButton = ({
  icon,
  text,
  onPress,
  customStyle,
}) => {
  const textMargin = icon ? {} : { paddingRight: dimensions.gutter }
  return (
    <TouchableOpacity onPress={onPress}>
      <View
        style={[styles.container, customStyle]}
      >
        <Text style={[styles.text, textMargin]}>{text}</Text>
        {icon && (<Image source={icon} style={styles.icon} />)}
      </View>
    </TouchableOpacity>
  )
}

const styles = StyleSheet.create({
  container: {
    flexDirection: 'row',
    backgroundColor: colors.theme.primary,
    height: dimensions.theme.buttonHeight,
    borderRadius: dimensions.theme.buttonHeight / 2,
    justifyContent: 'center',
    alignItems: 'center',
  },
  text: {
    fontSize: fonts.sizes.h3,
    color: colors.white,
    paddingLeft: dimensions.gutter,
  },
  icon: {
    marginLeft: dimensions.margin,
    marginRight: dimensions.gutter,
    width: 20,
    height: 20,
  },
})

export default PrimaryButton
