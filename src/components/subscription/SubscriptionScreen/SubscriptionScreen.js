import React from 'react'
import { TouchableHighlight, View, Image, Dimensions, Text, StyleSheet } from 'react-native'
import { NavigationActions } from 'react-navigation'

import HeaderBg from '../../../assets/icons/header_bg.png'
import Logo from '../../../assets/icons/logo.png'
import Close from '../../../assets/icons/close.png'
import fonts from '../../../constants/fonts'
import colors from '../../../constants/colors'
import dimensions from '../../../constants/dimensions'
import * as routeNames from '../../../constants/route-names'

const LOGO_SIZE = 120
const WINDOW_WIDTH = Dimensions.get('window').width

const SubscriptionScreen = ({ dispatch }) => (
  <View style={styles.wrapper}>
    <Image
      source={HeaderBg}
      style={{ width: WINDOW_WIDTH, height: 200, position: 'absolute', top: 0, left: 0 }}
      resizeMode="stretch"
    />
    <TouchableHighlight
      underlayColor="transparent"
      onPress={() => dispatch(NavigationActions.navigate({ routeName: routeNames.App }))}
    >
      <Image
        source={Close}
        style={styles.close}
      />
    </TouchableHighlight>

    <Image
      source={Logo}
      style={styles.logo}
    />
    <View style={styles.infoContainer}>
      <Text style={styles.largeText}>
        Premium Access
      </Text>
      <View style={{
          width: WINDOW_WIDTH - (dimensions.largeMargin * 2),
          marginBottom: dimensions.gutter,
        }}
      >
        <Text style={styles.subText}>
          Choose a privileged subscription package for unlimited content !
        </Text>
      </View>
      <TouchableHighlight
        underlayColor="transparent"
        onPress={() => {}}
      >
        <View style={[styles.button, styles.buttonTrial]}>
          <Text style={[styles.buttonText]}>TRY FOR FREE</Text>
          <Text style={[styles.buttonText, { fontSize: fonts.sizes.h4 }]}>
            After the 3 day trial, then $7.99/monthly
          </Text>
        </View>
      </TouchableHighlight>
      <TouchableHighlight
        underlayColor="transparent"
        onPress={() => {}}
      >
        <View style={[styles.button, styles.buttonPremium]}>
          <Text style={[styles.buttonText]}>ANNUAL PLAN</Text>
          <Text style={[styles.buttonText, { fontSize: fonts.sizes.h4 }]}>
            $49.99/yearly
          </Text>
        </View>
      </TouchableHighlight>
      <TouchableHighlight
        underlayColor="transparent"
        onPress={() => {}}
      >
        <Text
          style={[styles.subText, { marginTop: dimensions.gutter, color: colors.transparentBlack80 }]}
        >Restore Purchases
        </Text>
      </TouchableHighlight>
    </View>
  </View>
)

const styles = StyleSheet.create({
  wrapper: {
    flex: 1,
    backgroundColor: colors.white,
  },
  close: {
    width: 20,
    height: 20,
    position: 'absolute',
    top: 30,
    left: dimensions.gutter,
    tintColor: colors.white,
  },
  logo: {
    width: LOGO_SIZE,
    height: LOGO_SIZE,
    position: 'absolute',
    left: (WINDOW_WIDTH / 2) - (LOGO_SIZE / 2),
    top: 120,
  },
  infoContainer: {
    position: 'absolute',
    top: 260,
    left: 0,
    bottom: 0,
    right: 0,
    justifyContent: 'flex-start',
    alignItems: 'center',
  },
  largeText: {
    fontSize: fonts.sizes.h1,
    color: colors.transparentBlack80,
    textAlign: 'center',
  },
  subText: {
    marginTop: dimensions.gutter,
    fontSize: fonts.sizes.h3,
    color: colors.transparentBlack50,
    textAlign: 'center',
  },
  button: {
    marginTop: dimensions.margin,
    borderRadius: 5,
    padding: dimensions.margin,
    width: WINDOW_WIDTH - dimensions.largeMargin,
    height: 50,
  },
  buttonTrial: {
    backgroundColor: colors.theme.trial,
  },
  buttonPremium: {
    backgroundColor: colors.theme.primary,
  },
  buttonText: {
    color: colors.white,
    textAlign: 'center',
  },
})

export default SubscriptionScreen
