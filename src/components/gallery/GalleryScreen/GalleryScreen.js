import React from 'react'
import Permissions from 'react-native-permissions'
import {
  View,
  CameraRoll,
  StyleSheet,
} from 'react-native'
import { NavigationActions } from 'react-navigation'
import { setSelectedPhotoUriAction } from '../../../redux/modules/gallery/gallery'
import PhotoGrid from '../../common/PhotoGrid/PhotoGrid'
import * as routeNames from '../../../constants/route-names'

const PHOTOS_PER_ROW = 4
const DISPLAY_FIRST = 500

class GalleryScreen extends React.Component {
  constructor(props) {
    super(props)
    this.state = {
      isLoading: false,
      photos: [],
      endCursor: null,
      hasNextPage: true,
    }
    this.onPhotoSelect = this.onPhotoSelect.bind(this)
  }

  componentDidMount() {
    Permissions.check('photo').then((response) => {
      // Response is one of: 'authorized', 'denied', 'restricted', or 'undetermined'
      if (response === 'undetermined') {
        Permissions.request('photo').then((res) => {
          if (res === 'authorized') {
            this.loadPhotos(DISPLAY_FIRST, null)
          }
        })
      } else if (response === 'authorized') {
        this.loadPhotos(DISPLAY_FIRST, null)
      }
    })
  }

  onPhotoSelect = (e, uri) => {
    e.stopPropagation()
    const { dispatch } = this.props
    dispatch(setSelectedPhotoUriAction(uri))
    dispatch(NavigationActions.navigate({ routeName: routeNames.Design }))
  }

  onEndReached = () => {
    this.loadPhotos(DISPLAY_FIRST, this.state.endCursor)
  }

  loadPhotos = (first, after) => {
    const getPhotosOption = {
      first,
      assetType: 'Photos',
    }
    if (this.state.hasNextPage && !this.state.isLoading) {
      this.setState({
        isLoading: true,
      })
      CameraRoll
        .getPhotos(after ? { ...getPhotosOption, after } : getPhotosOption)
        .then((r) => {
          this.setState({
            isLoading: false,
            photos: [...this.state.photos, ...r.edges],
            endCursor: r.page_info.end_cursor,
            hasNextPage: r.page_info.has_next_page,
          })
        })
    }
  }

  render() {
    return (
      <View style={styles.container}>
        <PhotoGrid
          isCameraRoll
          numColumns={PHOTOS_PER_ROW}
          photos={this.state.photos}
          onPhotoSelect={this.onPhotoSelect}
          onEndReached={this.onEndReached}
          isLoading={this.state.isLoading}
          onRefresh={() => this.loadPhotos(DISPLAY_FIRST, null)}
        />
      </View>
    )
  }
}


const styles = StyleSheet.create({
  container: {
    flex: 1,
  },
})


export default GalleryScreen
