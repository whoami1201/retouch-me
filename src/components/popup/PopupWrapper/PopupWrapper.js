import React from 'react'
import { Modal, Image, TouchableHighlight, Text, View, StyleSheet, Dimensions } from 'react-native'
import Close from '../../../assets/icons/close.png'
import colors from '../../../constants/colors'
import dimensions from '../../../constants/dimensions'
import fonts from '../../../constants/fonts'

class PopupWrapper extends React.PureComponent {
  render() {
    const {
      title,
      description,
      isVisible,
      renderCustomContent,
      onButtonPress,
      isModalAction,
      buttonText,
      onClose,
    } = this.props
    const actionButtonStyle = {
      backgroundColor: colors.theme.actionButton,
    }
    const buttonCustomStyle = isModalAction ? actionButtonStyle : {}
    const buttonTextStyle = isModalAction ? { color: colors.white } : {}

    return (
      <Modal
        animationType="fade"
        transparent
        visible={isVisible}
        onRequestClose={() => {}}
      >
        <View style={styles.container}>
          <View style={styles.modal}>
            <View
              style={styles.contentContainer}
            >
              {renderCustomContent
              ? renderCustomContent()
              : (
                <View>
                  <Text style={styles.title}>{title}</Text>
                  <Text style={styles.description}>
                    {description}
                  </Text>
                </View>
              )}
            </View>
            <TouchableHighlight
              underlayColor={colors.transparentBlack10}
              onPress={onButtonPress}
            >
              <View style={[styles.buttonContainer, buttonCustomStyle]}>
                <Text style={[styles.buttonText, buttonTextStyle]}>
                  {buttonText || 'OK' }
                </Text>
              </View>
            </TouchableHighlight>
            {isModalAction &&
              (
                <View style={styles.closeButton}>
                  <TouchableHighlight
                    onPress={onClose}
                  >
                    <Image
                      source={Close}
                      style={styles.closeIcon}
                    />
                  </TouchableHighlight>
                </View>
              )
            }
          </View>
        </View>
      </Modal>
    )
  }
}


const styles = StyleSheet.create({
  container: {
    backgroundColor: colors.theme.modalBackground,
    flex: 1,
    flexDirection: 'column',
    justifyContent: 'center',
    alignItems: 'center',
  },
  modal: {
    backgroundColor: colors.white,
    width: Dimensions.get('window').width - dimensions.largeMargin,
    borderRadius: 10,
    flexDirection: 'column',
    alignItems: 'center',
    justifyContent: 'center',
    overflow: 'hidden',
  },
  contentContainer: {
    padding: dimensions.largeMargin,
  },
  buttonContainer: {
    justifyContent: 'center',
    paddingTop: 12,
    paddingBottom: 12,
    borderTopWidth: 1,
    borderTopColor: colors.transparentBlack10,
    alignItems: 'center',
    width: Dimensions.get('window').width - dimensions.largeMargin,
  },
  buttonText: {
    color: colors.theme.primary,
    fontSize: fonts.sizes.h3,
  },
  closeButton: {
    position: 'absolute',
    right: dimensions.gutter,
    top: dimensions.gutter,
  },
  closeIcon: {
    width: 16,
    height: 16,
  },
  title: {
    marginBottom: dimensions.gutter,
    fontSize: fonts.sizes.h3,
    textAlign: 'center',
  },
  description: {
    marginBottom: dimensions.smallMargin,
    fontSize: fonts.sizes.h4,
    textAlign: 'center',
  },
})

export default PopupWrapper
