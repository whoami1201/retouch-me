import React from 'react'
import {
  StyleSheet,
  View,
  Text,
  TouchableNativeFeedback,
  TouchableHighlight,
  Platform,
  Image,
} from 'react-native'

import closeIcon from '../../../assets/icons/close_icon.png'
import colors from '../../../constants/colors'
import { removeModelAction, updateCurrentModelIdAction } from '../../../redux/modules/design/design'

const DesignSelectBox = (props) => {
  const {
    canvasWidth,
    canvasHeight,
    models,
    dispatch,
    currentModelId,
  } = props
  const container = {
    width: canvasWidth,
    height: canvasHeight,
  }
  const iconSize = 20
  const closeButtonStyle = (boxSize) => ({
    position: 'absolute',
    top: -(iconSize / 2),
    left: boxSize - 16,
    width: iconSize,
    height: iconSize,
  })
  const containerStyle = (model) => {
    const { location, boxSize, id } = model
    return ({
      left: location.x < 0 ? 0 : location.x,
      top: location.y < 0 ? 0 : location.y,
      width: boxSize,
      height: boxSize,
      borderColor: (id === currentModelId) ? colors.theme.primary : colors.transparentBlack10,
    })
  }
  const idStyle = (model) => ({
    position: 'absolute',
    top: model.boxSize - 24,
    left: model.boxSize - 20,
    color: colors.white,
  })
  const TouchComponent = Platform.OS === 'ios' ? TouchableHighlight : TouchableNativeFeedback

  return (
    <View style={container}>
      {models.map((model) => (
        <TouchComponent
          key={model.id}
          onPress={() => dispatch(updateCurrentModelIdAction(model.id))}
        >
          <View
            style={[styles.boxContainer, containerStyle(model)]}
          >
            <TouchComponent
              onPress={() => dispatch(removeModelAction(model.id))}
            >
              <Image
                style={closeButtonStyle(model.boxSize)}
                source={closeIcon}
              />
            </TouchComponent>
            <Text style={idStyle(model)}>{model.id}</Text>
          </View>
        </TouchComponent>
      ))}
    </View>
  )
}

const styles = StyleSheet.create({
  boxContainer: {
    position: 'absolute',
    backgroundColor: colors.transparentWhite40,
    borderWidth: 2,
    borderStyle: 'dashed',
    borderRadius: 2,
  },
})


export default DesignSelectBox
