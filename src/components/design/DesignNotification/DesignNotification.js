import React from 'react'
import { Modal, Image, Text, TouchableWithoutFeedback, View, StyleSheet, Dimensions } from 'react-native'

import TouchIcon from '../../../assets/icons/touch_icon.png'
import colors from '../../../constants/colors'
import dimensions from '../../../constants/dimensions'
import { updateNotificationVisibleAction } from '../../../redux/modules/design/design'
import fonts from '../../../constants/fonts'

const DesignNotification = (props) => {
  const { isVisible, dispatch } = props
  return (
    <Modal
      animationType="fade"
      transparent
      visible={isVisible}
      onRequestClose={() => {}}
    >
      <TouchableWithoutFeedback
        onPress={() => { dispatch(updateNotificationVisibleAction(false)) }}
      >
        <View style={styles.container}>
          <TouchableWithoutFeedback
            onPress={() => {}}
          >
            <View style={styles.modal}>
              <Image source={TouchIcon} style={styles.touch} />
              <Text style={styles.mainText}>Select area to touch</Text>
              <Text style={styles.subText}>
                Click on person to select him or her to retouch.
              </Text>
              <Text style={styles.subText}>
                And then choose retouch option for this person
              </Text>
            </View>
          </TouchableWithoutFeedback>

        </View>
      </TouchableWithoutFeedback>
    </Modal>
  )
}


const styles = StyleSheet.create({
  container: {
    backgroundColor: colors.theme.modalBackground,
    flex: 1,
    flexDirection: 'column',
    justifyContent: 'flex-end',
    alignItems: 'center',
  },
  modal: {
    backgroundColor: colors.white,
    width: Dimensions.get('window').width,
    height: 220,
    borderRadius: 10,
    flexDirection: 'column',
    alignItems: 'center',
  },
  touch: {
    marginTop: dimensions.largeMargin,
    marginBottom: dimensions.gutter,
    width: 64,
    height: 64,
  },
  mainText: {
    marginBottom: dimensions.gutter,
    fontSize: fonts.sizes.h3,
  },
  subText: {
    marginBottom: dimensions.smallMargin,
    fontSize: fonts.sizes.h4,
  },
})

export default DesignNotification
