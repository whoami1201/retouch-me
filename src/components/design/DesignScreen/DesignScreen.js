import React from 'react'
import {
  View,
  StyleSheet,
} from 'react-native'
import { NavigationActions } from 'react-navigation'

import DesignNotification from '../DesignNotification/DesignNotification'
import DesignFeatures from '../DesignFeatures/DesignFeatures'
import colors from '../../../constants/colors'
import DesignPhoto from '../DesignPhoto/DesignPhoto'
import { resetDesignState, uploadPhotoAction, updateCanvasSize } from '../../../redux/modules/design/design'
import PopupWrapper from '../../popup/PopupWrapper/PopupWrapper'
import FeatureSummary from '../../common/FeatureSummary/FeatureSummary'
import PhotoStatus from '../../common/PhotoStatus/PhotoStatus'
import * as routeNames from '../../../constants/route-names'

const MODAL_ORDER = 'order'
const MODAL_STATUS = 'status'
class DesignScreen extends React.PureComponent {
  constructor(props) {
    super(props)
    this.state = {
      photoWidth: null,
      photoHeight: null,
      activeModal: null,
    }
    this.setActiveModal = this.setActiveModal.bind(this)
    this.renderSummaryContent = this.renderSummaryContent.bind(this)
  }

  componentWillReceiveProps(nextProps) {
    const { designState } = nextProps
    if (designState.uploadResult &&
      this.props.designState.uploadResult !== designState.uploadResult) {
      this.setActiveModal(MODAL_STATUS)
    }
  }

  componentWillUnmount() {
    this.props.dispatch(resetDesignState())
  }

  setPhotoDimension = (width, height) => {
    this.setState({
      photoWidth: width,
      photoHeight: height,
    })
    this.props.dispatch(updateCanvasSize({ width, height }))
  }

  setActiveModal = (type) => {
    this.setState({ activeModal: type })
  }

  renderSummaryContent = () => {
    const { designState } = this.props
    return (
      <FeatureSummary
        models={designState.models}
        categories={designState.categories}
      />
    )
  }

  renderStatusContent = () => {
    const { designState } = this.props
    return (
      <PhotoStatus
        photo={designState.uploadResult}
      />
    )
  }

  render() {
    const { selectedPhotoUri, designState, dispatch } = this.props
    const { photoWidth, photoHeight, activeModal } = this.state
    const {
      categories,
      models,
      currentModelId,
      isNotificationVisible,
    } = designState
    const orderModalProps = {
      isVisible: activeModal === MODAL_ORDER,
      isModalAction: true,
      renderCustomContent: this.renderSummaryContent,
      buttonText: 'Continue',
      onButtonPress: () => {
        dispatch(uploadPhotoAction(selectedPhotoUri))
        this.setActiveModal(MODAL_STATUS)
      },
      onClose: () => this.setActiveModal(null),
    }
    const statusModalProps = {
      isVisible: activeModal === MODAL_STATUS,
      renderCustomContent: this.renderStatusContent,
      onButtonPress: () => {
        this.setActiveModal(null)
        this.props.dispatch(NavigationActions.navigate({ routeName: routeNames.Ready }))
      },
      onClose: () => this.setActiveModal(null),
    }
    const modalProps = activeModal === MODAL_ORDER
      ? orderModalProps
      : statusModalProps

    if (categories.length > 0) {
      return (
        <View style={styles.container}>
          <PopupWrapper
            {...modalProps}
          />
          <View
            style={styles.photoContainer}
            onLayout={(e) => {
              const { width, height } = e.nativeEvent.layout
              this.setPhotoDimension(width, height)
            }}
          >
            <DesignPhoto
              dispatch={dispatch}
              selectedPhotoUri={selectedPhotoUri}
              models={models}
              currentModelId={currentModelId}
              width={photoWidth}
              height={photoHeight}
              onPressSend={() => { this.setActiveModal(MODAL_ORDER) }}
            />
          </View>
          <View style={styles.featureContainer}>
            <DesignNotification
              dispatch={dispatch}
              isVisible={isNotificationVisible}
            />
            <DesignFeatures
              dispatch={dispatch}
              designState={designState}
            />
          </View>
        </View>
      )
    }
    return null
  }
}

const styles = StyleSheet.create({
  container: {
    backgroundColor: colors.theme.designBackground,
    flex: 1,
    flexDirection: 'column',
  },
  photoContainer: {
    flex: 2,
  },
  featureContainer: {
    flex: 1,
  },
})


export default DesignScreen
