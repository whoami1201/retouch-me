import React from 'react'
import { ImageBackground, TouchableHighlight, View, StyleSheet } from 'react-native'

import DesignSelectBox from '../DesignSelectBox/DesignSelectBox'
import dimensions from '../../../constants/dimensions'
import PrimaryButton from '../../buttons/PrimaryButton/PrimaryButton'
import ArrowRight from '../../../assets/icons/arrow_right.png'
import { addNewModelAction, DEFAULT_BOX_SIZE, getBoxDimension } from '../../../redux/modules/design/design'
import colors from '../../../constants/colors'

const DesignPhoto = (props) => {
  const {
    selectedPhotoUri,
    width,
    height,
    dispatch,
    currentModelId,
    models,
    onPressSend,
  } = props
  const showSendButton = (models.length > 0) &&
    !!(models.find((m) => m.selectedFeatures && m.selectedFeatures.length > 0) !== undefined)
  return (
    <ImageBackground
      source={{ uri: selectedPhotoUri }}
      style={styles.photo}
      resizeMode="contain"
    >
      <TouchableHighlight
        underlayColor={colors.transparent}
        onPress={(e) => {
          const { locationX, locationY } = e.nativeEvent
          const getLoc = (loc) => loc - (getBoxDimension(DEFAULT_BOX_SIZE) / 2)
          dispatch(addNewModelAction({
            x: getLoc(locationX),
            y: getLoc(locationY),
          }))
        }}
      >
        <DesignSelectBox
          canvasWidth={width}
          canvasHeight={height}
          currentModelId={currentModelId}
          models={models}
          dispatch={dispatch}
        />
      </TouchableHighlight>
      {
        showSendButton && (
          <View style={styles.sendButton}>
            <PrimaryButton
              text="Send to designer"
              icon={ArrowRight}
              onPress={onPressSend}
              customStyle={{ backgroundColor: colors.theme.secondary }}
            />
          </View>
        )
      }
    </ImageBackground>
  )
}


const styles = StyleSheet.create({
  photo: {
    flex: 1,
  },
  sendButton: {
    position: 'absolute',
    bottom: dimensions.gutter,
    right: dimensions.gutter,
  },
})

export default DesignPhoto
