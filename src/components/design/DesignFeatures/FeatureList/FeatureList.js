import React from 'react'
import {
  ScrollView,
  Text,
  TouchableHighlight,
  View,
  StyleSheet,
  Dimensions,
  Image,
} from 'react-native'
import colors from '../../../../constants/colors'
import dimensions from '../../../../constants/dimensions'
import { addSelectedFeature, removeSelectedFeature } from '../../../../redux/modules/design/design'
import fonts from '../../../../constants/fonts'

const FeatureList = (props) => {
  const { features, dispatch, model } = props
  return (
    <ScrollView
      horizontal
    >
      {features &&
        features.map((feature) => {
          const isSelected = model ? model.selectedFeatures.includes(feature.slug) : false

          return (
            <View
              key={feature.slug}
              style={[styles.wrapper, {
                backgroundColor: isSelected
                  ? colors.theme.primary
                  : colors.theme.darkFeatureBackground,
              }]}
            >
              <TouchableHighlight
                underlayColor={colors.transparent}
                onPress={() => {
                  if (isSelected) {
                    dispatch(removeSelectedFeature(feature.slug))
                  } else {
                    dispatch(addSelectedFeature(feature.slug))
                  }
                }}
              >
                <View style={styles.featureContainer}>
                  <Image
                    style={styles.featureImage}
                    source={{ uri: feature.placeholder_url }}
                  />
                  <Text style={styles.featureLabel}>{feature.name}</Text>
                </View>
              </TouchableHighlight>
            </View>
          )
        })
      }
    </ScrollView>
  )
}

const styles = StyleSheet.create({
  wrapper: {
    alignItems: 'stretch',
    justifyContent: 'center',
  },
  featureContainer: {
    paddingTop: dimensions.gutter,
    flexDirection: 'column',
    width: (Dimensions.get('window').width / 5) + dimensions.margin,
    justifyContent: 'space-around',
    alignItems: 'center',
  },
  featureImage: {
    borderRadius: 25,
    width: 50,
    height: 50,
  },
  featureLabel: {
    minHeight: 50,
    maxWidth: 60,
    paddingTop: dimensions.margin,
    textAlign: 'center',
    color: colors.white,
    fontSize: fonts.sizes.h4,
  },
})

export default FeatureList
