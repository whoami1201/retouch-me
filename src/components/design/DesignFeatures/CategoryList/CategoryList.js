import React from 'react'
import {
  ScrollView,
  Text,
  TouchableHighlight,
  View,
  StyleSheet,
  Dimensions,
  Image,
} from 'react-native'
import colors from '../../../../constants/colors'
import TabIcon from '../../../../assets/icons/tab_icon_example.png'
import dimensions from '../../../../constants/dimensions'
import { updateCurrentCategoryId } from '../../../../redux/modules/design/design'
import fonts from '../../../../constants/fonts'

const CategoryList = (props) => {
  const { categories, currentCategoryId, dispatch } = props
  const getActiveColor = (isActive) => (
    isActive
      ? colors.theme.primary
      : colors.grey
  )
  return (
    <ScrollView
      horizontal
    >
      {categories &&
        categories.map((category) => {
          const activeColor = getActiveColor(category.slug === currentCategoryId)
          return (
            <View
              style={styles.wrapper}
              key={category.slug}
            >
              <TouchableHighlight
                underlayColor={colors.white}
                onPress={() => dispatch(updateCurrentCategoryId(category.slug))}
              >
                <View style={styles.categoryContainer}>
                  <Image
                    source={TabIcon}
                    style={{ tintColor: activeColor }}
                  />
                  <Text
                    style={[styles.categoryLabel, { color: activeColor }]}
                  >{category.name}
                  </Text>
                </View>
              </TouchableHighlight>
            </View>
          )
        })}
    </ScrollView>
  )
}

const styles = StyleSheet.create({
  wrapper: {
    flex: 1,
    justifyContent: 'center',
    backgroundColor: colors.white,
  },
  categoryContainer: {
    flexDirection: 'column',
    width: Dimensions.get('window').width / 4,
    justifyContent: 'space-evenly',
    alignItems: 'center',
  },
  categoryLabel: {
    paddingTop: dimensions.margin,
    color: colors.grey,
    fontSize: fonts.sizes.h4,
  },
})

export default CategoryList
