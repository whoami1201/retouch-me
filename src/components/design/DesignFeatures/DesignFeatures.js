import React from 'react'
import {
  View,
  StyleSheet,
} from 'react-native'
import FeatureList from './FeatureList/FeatureList'
import CategoryList from './CategoryList/CategoryList'
import { updateCurrentCategoryId } from '../../../redux/modules/design/design'

class DesignFeatures extends React.PureComponent {
  componentDidMount() {
    this.props.dispatch(updateCurrentCategoryId('body'))
  }
  render() {
    const { dispatch, designState } = this.props
    const {
      categories,
      currentCategoryId,
      models,
      currentModelId,
    } = designState
    const { features } = currentCategoryId
      ? categories.find((category) => category.slug === currentCategoryId)
      : []
    const model = models.find((m) => m.id === currentModelId)
    return (
      <View style={styles.container}>
        <View
          style={styles.featureContainer}
        >
          <FeatureList
            model={model}
            features={features}
            dispatch={dispatch}
          />
        </View>
        <View
          style={styles.categoryContainer}
        >
          <CategoryList
            currentCategoryId={currentCategoryId}
            categories={categories}
            dispatch={dispatch}
          />
        </View>
      </View>
    )
  }
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    flexDirection: 'column',
    alignItems: 'stretch',
  },
  featureContainer: {
    flex: 2,
  },
  categoryContainer: {
    flex: 1,
  },
})

export default DesignFeatures
