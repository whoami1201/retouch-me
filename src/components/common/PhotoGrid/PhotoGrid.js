import React from 'react'
import {
  View,
  FlatList,
  StyleSheet,
  RefreshControl,
} from 'react-native'

import PhotoItem from '../PhotoItem/PhotoItem'
import ReadyPhotoItem from '../ReadyPhotoItem/ReadyPhotoItem'

const PhotoGrid = (props) => {
  const {
    onPhotoSelect,
    numColumns,
    photos,
    isCameraRoll,
    onEndReached,
    isLoading,
    onRefresh,
  } = props

  const keyExtractor = (photo) => (
    isCameraRoll
      ? photo.node.image.uri
      : photo.id
  )

  const renderItem = (item) => {
    const ComponentName = isCameraRoll ? PhotoItem : ReadyPhotoItem
    return (
      <ComponentName
        item={item}
        numColumns={numColumns}
        onPhotoSelect={onPhotoSelect}
      />
    )
  }

  return (
    <View style={styles.container}>
      <FlatList
        removeClippedSubviews
        contentContainerStyle={styles.flatList}
        onEndReached={onEndReached}
        onEndReachedThreshold={0.5}
        data={photos}
        refreshControl={
          <RefreshControl
            refreshing={isLoading}
            onRefresh={onRefresh}
          />
        }
        numColumns={numColumns}
        keyExtractor={keyExtractor}
        renderItem={({ item }) => renderItem(item)}
      />
    </View>
  )
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
  },
  flatList: {
    flexDirection: 'column',
  },
})


export default PhotoGrid
