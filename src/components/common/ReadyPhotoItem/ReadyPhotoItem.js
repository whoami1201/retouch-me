import React from 'react'
import {
  Dimensions,
  ImageBackground,
  Text,
  View,
  Image,
  TouchableHighlight,
  StyleSheet,
} from 'react-native'

import RetouchPhotoIcon from '../../../assets/icons/retouch-photo.png'
import colors from '../../../constants/colors'
import dimensions from '../../../constants/dimensions'
import { resizePhotoUrl } from '../../../utils'
import fonts from '../../../constants/fonts'

const ReadyPhotoItem = (props) => {
  const {
    item,
    onPhotoSelect,
    numColumns,
  } = props
  const isProcessed = item.is_processed
  const { width } = Dimensions.get('window')
  const photoDimension = (width / numColumns) - dimensions.smallMargin
  const photoStyle = {
    width: photoDimension,
    height: photoDimension,
    margin: dimensions.smallMargin / 2,
  }

  return (
    <TouchableHighlight
      key={`touch-${item.cloudinary_public_id}`}
      underlayColor={colors.transparent}
      onPress={(e) => onPhotoSelect(e, item)}
    >
      <ImageBackground
        style={photoStyle}
        source={{ uri: resizePhotoUrl(item.img_processed_url || item.img_uploaded_url) }}
      >
        {
          isProcessed
            ? null
            : (
              <View style={styles.overlayContainer}>
                <View
                  style={styles.overlay}
                >
                  <Image source={RetouchPhotoIcon} style={styles.icon} />
                  <Text style={styles.text}>Photo is being retouched.</Text>
                  <Text style={styles.text}>Click here to view details.</Text>
                </View>
                <View style={styles.buttonWrapper}>
                  <View style={styles.buttonContainer}>
                    <Text style={styles.text}>{item.status}</Text>
                  </View>
                </View>
              </View>
            )
        }
      </ImageBackground>
    </TouchableHighlight>
  )
}

const styles = StyleSheet.create({
  overlayContainer: {
    flex: 1,
    backgroundColor: colors.transparentBlack50,
  },
  overlay: {
    flex: 2,
    alignItems: 'center',
    justifyContent: 'center',
  },
  text: {
    color: colors.white,
    fontSize: fonts.sizes.h4,
  },
  icon: {
    width: 32,
    height: 32,
    marginBottom: dimensions.gutter,
  },
  buttonWrapper: {
    alignItems: 'flex-end',
  },
  buttonContainer: {
    width: 80,
    height: 20,
    alignItems: 'center',
    justifyContent: 'center',
    backgroundColor: colors.theme.primary,
    marginBottom: dimensions.margin,
    marginRight: dimensions.margin,
    borderRadius: 10,
  },
})

export default ReadyPhotoItem
