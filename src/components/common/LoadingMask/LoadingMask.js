import React from 'react'
import { ActivityIndicator, View, StyleSheet } from 'react-native'

const LoadingMask = (props) => (
  <View style={[styles.container, props.style]}>
    <ActivityIndicator />
  </View>
)

const styles = StyleSheet.create({
  container: {
    position: 'absolute',
    top: 0,
    left: 0,
    right: 0,
    bottom: 0,
    justifyContent: 'center',
  },
})

export default LoadingMask
