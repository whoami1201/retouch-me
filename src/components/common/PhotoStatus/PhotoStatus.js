import React from 'react'
import { View, StyleSheet, Text } from 'react-native'
import StepIndicator from '../../common/StepIndicator/StepIndicator'
import dimensions from '../../../constants/dimensions'
import fonts from '../../../constants/fonts'

const STATUS = ['queue', 'editing', 'completed']
const STATUS_TEXTS = [
  'Your photo is in queue.',
  'Your photo is being retouched',
  'Your photo is ready',
]

const PhotoStatus = ({
  photo,
}) => {
  const statusIndex = photo && STATUS.indexOf(photo.status)

  if (!photo) {
    return (
      <View>
        <Text style={styles.title}>Placing Order</Text>
        <Text style={styles.description}>
          We are sending your request to our designers, please wait.
        </Text>
      </View>
    )
  }
  return (
    <View style={styles.container}>
      <Text style={styles.title}>Status</Text>
      <Text style={styles.description}>
        {STATUS_TEXTS[statusIndex]}
      </Text>
      <View style={styles.steps}>
        <StepIndicator
          currentStatus={photo.status}
          labels={STATUS}
        />
      </View>
    </View>
  )
}

const styles = StyleSheet.create({
  container: {
    height: 120,
    width: 250,
  },
  steps: {
    marginTop: dimensions.gutter,
    flex: 1,
  },
  title: {
    marginBottom: dimensions.gutter,
    fontSize: fonts.sizes.h3,
    textAlign: 'center',
  },
  description: {
    marginBottom: dimensions.smallMargin,
    fontSize: fonts.sizes.h4,
    textAlign: 'center',
  },
})

export default PhotoStatus
