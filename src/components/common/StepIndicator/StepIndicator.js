import React from 'react'
import { View, StyleSheet, Text } from 'react-native'
import dimensions from '../../../constants/dimensions'
import colors from '../../../constants/colors'
import fonts from '../../../constants/fonts'

const COLOR_CURRENT = colors.theme.progress
const COLOR_FINISH = colors.theme.progress
const COLOR_UNFINISH = colors.transparentBlack10

const getBackground = (bar, index) => {
  if (bar === index) {
    return styles.bgCurrent
  } else if (bar < index) {
    return styles.bgFinish
  }
  return styles.bgUnfinish
}

const getBarStyle = (bar, arrayLength) => {
  switch (bar) {
    case 0:
      return styles.firstBar

    case arrayLength - 1:
      return styles.lastBar

    default:
      return styles.normalBar
  }
}

const StepIndicator = ({
  labels,
  currentStatus,
}) => {
  const currentIndex = labels.indexOf(currentStatus)
  return (
    <View style={styles.container}>
      {labels.map((label) => {
        const bar = labels.indexOf(label)
        return (
          <View
            key={label}
            style={styles.labelContainer}
          >
            <View
              style={[
                styles.bar,
                getBarStyle(bar, labels.length),
                getBackground(bar, currentIndex),
              ]}
            />
            <Text style={styles.label}>{label}</Text>
          </View>
        )
      })}
    </View>
  )
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    flexDirection: 'row',
  },
  labelContainer: {
    flex: 1,
    flexDirection: 'column',
    height: 60,
  },
  label: {
    marginTop: dimensions.margin,
    alignSelf: 'center',
    fontSize: fonts.sizes.h4,
  },
  bar: {
    height: dimensions.gutter,
    borderRightWidth: 1,
    borderColor: colors.white,
  },
  firstBar: {
    borderBottomLeftRadius: dimensions.gutter / 2,
    borderTopLeftRadius: dimensions.gutter / 2,
  },
  lastBar: {
    borderBottomRightRadius: dimensions.gutter / 2,
    borderTopRightRadius: dimensions.gutter / 2,
  },
  bgUnfinish: {
    backgroundColor: COLOR_UNFINISH,
  },
  bgFinish: {
    backgroundColor: COLOR_FINISH,
  },
  bgCurrent: {
    backgroundColor: COLOR_CURRENT,
  },
})

export default StepIndicator
