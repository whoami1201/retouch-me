import React from 'react'
import { connect } from 'react-redux'
import {
  View,
  Text,
  Image,
  StyleSheet,
} from 'react-native'

import userIcon from '../../../assets/icons/users.png'

import colors from '../../../constants/colors'
import dimensions from '../../../constants/dimensions'
import fonts from '../../../constants/fonts'

const UserCountIcon = (props) => {
  const {
    userCount,
  } = props

  return (
    <View style={styles.container}>
      <Image
        source={userIcon}
        resizeMode="contain"
        style={styles.image}
      />
      <View
        style={styles.textContainer}
      >
        <Text
          style={styles.text}
        >
          {userCount}
        </Text>
      </View>
    </View>
  )
}

const styles = StyleSheet.create({
  container: {
    width: 32,
    height: 32,
    marginRight: dimensions.margin,
  },
  image: {
    position: 'absolute',
    top: 8,
    left: 0,
    height: 24,
    width: 24,
  },
  text: {
    fontSize: fonts.sizes.h4,
    color: colors.white,
  },
  textContainer: {
    position: 'absolute',
    top: 0,
    right: 0,
    height: 20,
    width: 20,
    backgroundColor: colors.theme.primary,
    borderRadius: 10,
    alignItems: 'center',
    justifyContent: 'center',
  },
})

const mapStateToProps = ({
  designState,
}) => ({
  userCount: designState.models.length,
})

export default connect(mapStateToProps)(UserCountIcon)
