import React from 'react'
import {
  Dimensions,
  Image,
  TouchableHighlight,
} from 'react-native'

import colors from '../../../constants/colors'
import dimensions from '../../../constants/dimensions'

const PhotoItem = (props) => {
  const {
    item,
    onPhotoSelect,
    numColumns,
  } = props
  const { width } = Dimensions.get('window')
  const photoDimension = (width / numColumns) - dimensions.smallMargin
  const photoStyle = {
    width: photoDimension,
    height: photoDimension,
    margin: dimensions.smallMargin / 2,
  }

  return (
    <TouchableHighlight
      key={`touch-${item.node.image.uri}`}
      underlayColor={colors.transparent}
      onPress={(e) => onPhotoSelect(e, item.node.image.uri)}
    >
      <Image
        key={item.node.image.uri}
        style={photoStyle}
        source={{ uri: item.node.image.uri }}
      />
    </TouchableHighlight>
  )
}

export default PhotoItem
