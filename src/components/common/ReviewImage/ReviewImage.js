import React, { Component } from 'react'
import { TouchableWithoutFeedback, Image, StyleSheet } from 'react-native'

const AFTER_IMG = 'after'
const BEFORE_IMG = 'before'

class ReviewImage extends Component {
  constructor(props) {
    super(props)
    this.state = {
      currentDisplay: AFTER_IMG,
    }
    this.updateCurrentDisplay = this.updateCurrentDisplay.bind(this)
  }

  updateCurrentDisplay = (currentDisplay) => (
    this.setState({ currentDisplay })
  )

  render() {
    const { currentDisplay } = this.state
    const {
      beforePhotoUri,
      afterPhotoUri,
      onPressIn,
      onPressOut,
    } = this.props
    const displayStyle = (type) => ({
      display: currentDisplay === type ? 'flex' : 'none',
    })

    return (
      <TouchableWithoutFeedback
        style={{ flex: 1 }}
        onPressIn={() => {
          this.updateCurrentDisplay(BEFORE_IMG)
          !!onPressIn && onPressIn() // eslint-disable-line
        }}
        onPressOut={() => {
          this.updateCurrentDisplay(AFTER_IMG)
          !!onPressOut && onPressOut() // eslint-disable-line
        }}
      >
        {
          currentDisplay === AFTER_IMG
            ? <Image
              source={{ uri: afterPhotoUri }}
              style={[styles.photo, displayStyle(AFTER_IMG)]}
              resizeMode="contain"
            />
            : <Image
              source={{ uri: beforePhotoUri }}
              style={[styles.photo, displayStyle(BEFORE_IMG)]}
              resizeMode="contain"
            />
        }
      </TouchableWithoutFeedback>
    )
  }
}

const styles = StyleSheet.create({
  photo: {
    display: 'flex',
    alignSelf: 'stretch',
    flex: 1,
  },
})

export default ReviewImage
