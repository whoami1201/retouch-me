import React from 'react'
import { View, StyleSheet, Text } from 'react-native'
import dimensions from '../../../constants/dimensions'
import colors from '../../../constants/colors'
import fonts from '../../../constants/fonts'

const FeatureSummary = ({
  models,
  categories,
}) => {
  const featureNames = categories.reduce((arr, category) => {
    category.features.forEach((feature) => {
      arr.push(feature)
    })
    return arr
  }, [])
  return (
    <View>
      <Text style={styles.title}>Your Order</Text>
      <Text style={styles.description}>
        Below is a summary of your selected features for this photo.
      </Text>
      <View style={styles.featureContainer}>
        {models.map((model) => {
          const selectedNames = model.selectedFeatures.map((selectedFeature) => {
            const feature = featureNames.find((f) => selectedFeature === f.slug)
            return feature
              ? (
                <Text
                  key={`${model.id}-${feature.slug}`}
                  style={styles.feature}
                >{feature.name}
                </Text>)
              : ''
          })

          return (
            <View
              key={`model-${model.id}`}
              style={{ marginBottom: dimensions.margin }}
            >
              <Text style={[styles.person, styles.feature]}>Person {model.id}
              </Text>
              {selectedNames}
            </View>
          )
        })}
      </View>
    </View>
  )
}

const styles = StyleSheet.create({
  featureContainer: {
    marginTop: dimensions.gutter,
    paddingTop: dimensions.margin,
    borderTopWidth: 1,
    borderBottomWidth: 1,
    borderColor: colors.transparentBlack10,
  },
  title: {
    marginBottom: dimensions.gutter,
    fontSize: fonts.sizes.h3,
    textAlign: 'center',
  },
  description: {
    marginBottom: dimensions.smallMargin,
    fontSize: fonts.sizes.h4,
    textAlign: 'center',
  },
  person: {
    fontWeight: 'bold',
  },
  feature: {
    textAlign: 'center',
    marginBottom: dimensions.smallMargin,
  },
})

export default FeatureSummary
