import { StyleSheet, View, Text } from 'react-native'
import { connect } from 'react-redux'
import React from 'react'

import colors from '../../../constants/colors'

const GalleryHeader = () => (
  <View style={styles.headerContainer}>
    <View style={styles.side} />
    <View><Text>Roll</Text></View>
    <View style={styles.side} />
  </View>
)

const styles = StyleSheet.create({
  headerContainer: {
    paddingTop: 30,
    justifyContent: 'space-between',
    alignItems: 'center',
    backgroundColor: colors.white,
    flexDirection: 'row',
  },
  side: {
    flex: 1,
  },
})

const mapStateToProps = ({
  dispatch,
}) => ({
  dispatch,
})

export default connect(mapStateToProps)(GalleryHeader)
