import React from 'react'
import { NavigationActions } from 'react-navigation'
import { View, Text, Image, StyleSheet, Linking, StatusBar } from 'react-native'
import SettingsList from 'react-native-settings-list'
import { SocialIcon } from 'react-native-elements'
import icon from '../../../assets/icons/logo.png'
import colors from '../../../constants/colors'
import * as routeNames from '../../../constants/route-names'
import dimensions from '../../../constants/dimensions'
import fonts from '../../../constants/fonts'

const SettingsScreen = ({ info, dispatch, links }) => (
  <View style={styles.wrapper}>
    <StatusBar
      barStyle="light-content"
      backgroundColor={colors.theme.primary}
    />
    <SettingsList borderColor={colors.transparentBlack10} defaultItemSize={50}>
      <SettingsList.Header headerStyle={styles.headerStyle} />
      <SettingsList.Item
        icon={
          <Image
            style={styles.imageStyle}
            defaultSource={icon}
            source={{ uri: info.picture.data.url }}
          />
        }
        title={info.name}
        hasNavArrow={false}
      />

      <SettingsList.Header headerStyle={styles.headerStyle} />
      <SettingsList.Item
        title="Work Examples"
        onPress={() => {
          dispatch(NavigationActions.navigate({ routeName: routeNames.Example }))
        }}
      />
      <SettingsList.Item
        title="Privacy Policy"
        onPress={() => { Linking.openURL(links.policy) }}
      />
      <SettingsList.Item
        title="Terms of Service"
        onPress={() => { Linking.openURL(links.term_of_use) }}
      />
      <SettingsList.Item
        title="Report a Mistake"
        onPress={() => { Linking.openURL(links.support) }}
      />
      <SettingsList.Item
        title="Manage Subscription"
        rightSideContent={
          <Text style={styles.subscription}>
            {info.subscriptionStatus || ''}
          </Text>
        }
        onPress={() => {}}
      />

      <SettingsList.Header
        headerText="Follow us"
        headerStyle={styles.headerStyle}
      />
      <SettingsList.Item
        icon={
          <SocialIcon
            raised={false}
            style={styles.imageStyle}
            type="facebook"
          />
        }
        title="Facebook"
        onPress={() => { Linking.openURL(links.home) }}
      />
      <SettingsList.Item
        icon={
          <SocialIcon
            raised={false}
            style={styles.imageStyle}
            type="instagram"
          />
        }
        title="Instagram"
        onPress={() => { Linking.openURL(links.home) }}
      />
    </SettingsList>
  </View>
)

const styles = StyleSheet.create({
  wrapper: {
    flex: 1,
  },
  imageStyle: {
    marginLeft: 15,
    marginRight: 20,
    alignSelf: 'center',
    width: 32,
    height: 32,
    borderRadius: 4,
    justifyContent: 'center',
  },
  subscription: {
    color: colors.theme.primary,
    fontSize: fonts.sizes.h3,
    marginTop: 14,
    marginRight: dimensions.gutter,
  },
  headerStyle: {
    color: colors.grey,
    marginTop: dimensions.gutter,
    marginLeft: dimensions.gutter,
  },
})

export default SettingsScreen
