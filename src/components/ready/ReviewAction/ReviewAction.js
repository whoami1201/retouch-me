import React from 'react'
import {
  ScrollView,
  Text,
  TouchableHighlight,
  View,
  StyleSheet,
  Dimensions,
  Image,
  Share,
} from 'react-native'
import colors from '../../../constants/colors'
import DownloadIcon from '../../../assets/icons/download.png'
import ShareIcon from '../../../assets/icons/share.png'
import RateIcon from '../../../assets/icons/rate.png'
import DeleteIcon from '../../../assets/icons/trash.png'
import dimensions from '../../../constants/dimensions'

import { downloadPhotoAction, deletePhotoAction, toggleRatingDialogAction } from '../../../redux/modules/photos/photos'
import fonts from '../../../constants/fonts'

const ReviewAction = ({
  dispatch,
  photoId,
  photoUri,
}) => (
  <ScrollView
    horizontal
  >
    <View
      style={styles.wrapper}
    >
      <TouchableHighlight
        underlayColor="transparent"
        onPress={() => { dispatch(downloadPhotoAction(photoUri)) }}
      >
        <View style={styles.categoryContainer}>
          <Image
            source={DownloadIcon}
            style={{ tintColor: colors.white }}
          />
          <Text
            style={[styles.categoryLabel, { color: colors.white }]}
          >Download
          </Text>
        </View>
      </TouchableHighlight>
    </View>
    <View
      style={styles.wrapper}
    >
      <TouchableHighlight
        underlayColor="transparent"
        onPress={() => {
          Share.share({ url: photoUri })
        }}
      >
        <View style={styles.categoryContainer}>
          <Image
            source={ShareIcon}
            style={{ tintColor: colors.white }}
          />
          <Text
            style={[styles.categoryLabel, { color: colors.white }]}
          >Share
          </Text>
        </View>
      </TouchableHighlight>
    </View>
    <View
      style={styles.wrapper}
    >
      <TouchableHighlight
        underlayColor="transparent"
        onPress={() => { dispatch(toggleRatingDialogAction()) }}
      >
        <View style={styles.categoryContainer}>
          <Image
            source={RateIcon}
            style={{ tintColor: colors.white }}
          />
          <Text
            style={[styles.categoryLabel, { color: colors.white }]}
          >Rate
          </Text>
        </View>
      </TouchableHighlight>
    </View>
    <View
      style={styles.wrapper}
    >
      <TouchableHighlight
        underlayColor="transparent"
        onPress={() => { dispatch(deletePhotoAction(photoId)) }}
      >
        <View style={styles.categoryContainer}>
          <Image
            source={DeleteIcon}
            style={{ tintColor: colors.white }}
          />
          <Text
            style={[styles.categoryLabel, { color: colors.white }]}
          >Delete
          </Text>
        </View>
      </TouchableHighlight>
    </View>
  </ScrollView>
)

const styles = StyleSheet.create({
  wrapper: {
    flex: 1,
    justifyContent: 'center',
    backgroundColor: colors.theme.primary,
  },
  categoryContainer: {
    flexDirection: 'column',
    width: Dimensions.get('window').width / 4,
    justifyContent: 'space-evenly',
    alignItems: 'center',
  },
  categoryLabel: {
    paddingTop: dimensions.margin,
    color: colors.grey,
    fontSize: fonts.sizes.h4,
  },
})

export default ReviewAction
