import React from 'react'
import { View, Text, StyleSheet, Image, TouchableHighlight } from 'react-native'
import RateBad from '../../../assets/icons/rate_bad.png'
import RateGood from '../../../assets/icons/rate_good.png'
import RateNeutral from '../../../assets/icons/rate_neutral.png'
import colors from '../../../constants/colors'
import dimensions from '../../../constants/dimensions'
import { toggleRatingDialogAction } from '../../../redux/modules/photos/photos'
import fonts from '../../../constants/fonts'

const RateDialog = ({ dispatch }) => (
  <View style={styles.container}>
    <Text style={styles.text}>How do you rate our designer&apos;s work?</Text>
    <View style={styles.rateContainer}>
      <TouchableHighlight
        underlayColor="transparent"
        onPress={() => dispatch(toggleRatingDialogAction())}
      >
        <View style={styles.rateItem}>
          <Image
            source={RateBad}
            style={styles.rateIcon}
          />
          <Text style={styles.rateText}>Bad</Text>
        </View>
      </TouchableHighlight>
      <TouchableHighlight
        underlayColor="transparent"
        onPress={() => dispatch(toggleRatingDialogAction())}
      >
        <View style={styles.rateItem}>
          <Image
            source={RateNeutral}
            style={styles.rateIcon}
          />
          <Text style={styles.rateText}>Average</Text>
        </View>
      </TouchableHighlight>
      <TouchableHighlight
        underlayColor="transparent"
        onPress={() => dispatch(toggleRatingDialogAction())}
      >
        <View style={styles.rateItem}>
          <Image
            source={RateGood}
            style={styles.rateIcon}
          />
          <Text style={styles.rateText}>Good</Text>
        </View>
      </TouchableHighlight>
    </View>
  </View>
)


const styles = StyleSheet.create({
  container: {
    height: 130,
  },
  rateContainer: {
    marginTop: dimensions.gutter,
    flexDirection: 'row',
    justifyContent: 'center',
    alignItems: 'center',
  },
  rateItem: {
    justifyContent: 'center',
    alignItems: 'center',
    margin: dimensions.margin,
  },
  text: {
    fontSize: fonts.sizes.h3,
  },
  rateIcon: {
    width: 64,
    height: 64,
    marginBottom: dimensions.margin,
  },
  rateText: {
    color: colors.transparentBlack50,
  },
})

export default RateDialog
