import React from 'react'
import {
  View,
  StyleSheet,
  Image,
  Text,
} from 'react-native'
import { resizePhotoUrl, RESIZE_W_800 } from '../../../utils'
import HoldIcon from '../../../assets/icons/hold.png'
import colors from '../../../constants/colors'
import dimensions from '../../../constants/dimensions'
import ReviewAction from '../ReviewAction/ReviewAction'
import ReviewImage from '../../common/ReviewImage/ReviewImage'
import fonts from '../../../constants/fonts'

const BEFORE_IMG = 'before'
const AFTER_IMG = 'after'

/* eslint-disable-next-line */
class ReviewScreen extends React.Component {
  constructor(props) {
    super(props)
    this.state = {
      currentDisplay: AFTER_IMG,
    }
    this.updateCurrentDisplay = this.updateCurrentDisplay.bind(this)
  }

  updateCurrentDisplay(currentDisplay) {
    this.setState({ currentDisplay })
  }

  render() {
    const {
      dispatch,
      photoId,
      beforePhoto,
      afterPhoto,
    } = this.props
    const { currentDisplay } = this.state

    return (
      <View style={styles.container}>
        <View style={styles.statusWrapper}>
          <View style={styles.statusContainer}>
            <Text style={styles.text}>{
              currentDisplay === BEFORE_IMG
                ? 'Original'
                : 'Retouched'
            }
            </Text>
          </View>
        </View>
        <View style={{ flex: 6 }}>
          <ReviewImage
            beforePhotoUri={resizePhotoUrl(beforePhoto, RESIZE_W_800)}
            afterPhotoUri={resizePhotoUrl(afterPhoto, RESIZE_W_800)}
            onPressIn={() => this.updateCurrentDisplay(BEFORE_IMG)}
            onPressOut={() => this.updateCurrentDisplay(AFTER_IMG)}
          />
        </View>
        <View style={styles.holdContainer}>
          <Image
            source={HoldIcon}
            style={styles.hold}
          />
          { currentDisplay === BEFORE_IMG
            ? <Text style={styles.textHold}>Release to see the retouched photo</Text>
            : <Text style={styles.textHold}>Press and hold to see the original photo</Text>}

        </View>
        <View style={styles.actionContainer}>
          <ReviewAction
            dispatch={dispatch}
            photoId={photoId}
            photoUri={afterPhoto}
          />
        </View>
      </View>
    )
  }
}


const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: colors.white,
  },
  hold: {
    width: 32,
    height: 32,
    tintColor: colors.theme.primary,
    marginBottom: dimensions.margin,
  },
  holdContainer: {
    flex: 2,
    marginTop: dimensions.margin,
    justifyContent: 'center',
    alignItems: 'center',
  },
  textHold: {
    fontSize: fonts.sizes.h3,
  },
  text: {
    color: colors.white,
  },
  actionContainer: {
    flex: 1,
    backgroundColor: colors.theme.primary,
  },
  statusWrapper: {
    alignItems: 'center',
  },
  statusContainer: {
    width: 100,
    height: 20,
    alignItems: 'center',
    justifyContent: 'center',
    backgroundColor: colors.theme.primary,
    marginTop: dimensions.margin,
    marginBottom: dimensions.margin,
    borderRadius: 10,
  },
})


export default ReviewScreen
