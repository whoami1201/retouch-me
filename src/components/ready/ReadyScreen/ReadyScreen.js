import React from 'react'
import {
  View,
  StyleSheet,
  Text,
} from 'react-native'
import { NavigationActions } from 'react-navigation'
import PhotoGrid from '../../common/PhotoGrid/PhotoGrid'
import { setCurrentViewPhoto } from '../../../redux/modules/photos/photos'
import * as routeNames from '../../../constants/route-names'
import PopupWrapper from '../../popup/PopupWrapper/PopupWrapper'
import PhotoStatus from '../../common/PhotoStatus/PhotoStatus'
import PrimaryButton from '../../buttons/PrimaryButton/PrimaryButton'
import dimensions from '../../../constants/dimensions'
import colors from '../../../constants/colors'
import fonts from '../../../constants/fonts'

const PHOTOS_PER_ROW = 2

class ReadyScreen extends React.Component {
  constructor(props) {
    super(props)
    this.state = {
      popupVisible: false,
      selectedPhoto: null,
    }
    this.onPhotoSelect = this.onPhotoSelect.bind(this)
    this.renderStatusContent = this.renderStatusContent.bind(this)
  }

  onPhotoSelect = (e, photo) => {
    const { dispatch } = this.props
    if (photo.is_processed) {
      dispatch(setCurrentViewPhoto(photo.id))
      dispatch(NavigationActions.navigate({ routeName: routeNames.Review }))
    } else {
      this.setState({
        popupVisible: true,
        selectedPhoto: photo,
      })
    }
  }

  renderStatusContent = () => (
    <PhotoStatus
      photo={this.state.selectedPhoto}
    />
  )

  render() {
    const { popupVisible } = this.state
    const {
      photos,
      isLoading,
      onRefresh,
      onEndReached,
    } = this.props

    if (photos.length === 0 && !isLoading) {
      return (
        <View style={styles.container}>
          <View style={styles.wrapper}>
            <Text style={styles.main}>Nothing here yet</Text>
            <Text style={styles.subText}>You have not sent photos to our designers.</Text>
            <Text style={styles.subText}>Do it now and get amazing result!</Text>
            <PrimaryButton
              text="Choose Photo"
              customStyle={styles.button}
            />
          </View>
        </View>
      )
    }
    return (
      <View style={styles.container}>
        <PopupWrapper
          isVisible={popupVisible}
          renderCustomContent={this.renderStatusContent}
          onButtonPress={() => {
            this.setState({
              popupVisible: false,
            })
          }}
        />
        <PhotoGrid
          numColumns={PHOTOS_PER_ROW}
          photos={photos}
          onPhotoSelect={this.onPhotoSelect}
          onEndReached={onEndReached}
          isLoading={isLoading}
          onRefresh={onRefresh}
        />
      </View>
    )
  }
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
  },
  wrapper: {
    flex: 1,
    justifyContent: 'center',
    alignItems: 'center',
  },
  main: {
    fontSize: fonts.sizes.h2,
    marginBottom: dimensions.gutter,
  },
  subText: {
    color: colors.transparentBlack50,
  },
  button: {
    marginTop: dimensions.largeMargin,
    width: 200,
    backgroundColor: colors.theme.secondary,
  },
})


export default ReadyScreen
