import React from 'react'
import { View } from 'react-native'
import { NavigationActions } from 'react-navigation'
import PopupWrapper from '../../popup/PopupWrapper/PopupWrapper'
import { setDownloadFinishedAction, setDeleteFinishedAction, toggleRatingDialogAction } from '../../../redux/modules/photos/photos'
import * as routeNames from '../../../constants/route-names'
import RateDialog from '../RateDialog/RateDialog'

const ReviewModals = (props) => {
  const {
    photosState,
    dispatch,
  } = props
  const {
    downloadSuccess,
    downloadFinished,
    deleteFinished,
    deleteSuccess,
    isRating,
  } = photosState
  const errorText = 'We could not perform this request at the moment. Please try again.'
  return (
    <View>
      <PopupWrapper
        isVisible={downloadFinished}
        title={downloadSuccess ? 'Download successful' : 'Download unsuccessful'}
        description={downloadSuccess ? 'The photo is now saved to your phone.' : errorText}
        onButtonPress={() =>
          dispatch(setDownloadFinishedAction({ isFinished: false, isSuccess: false }))}
      />
      <PopupWrapper
        isVisible={deleteFinished}
        title={deleteSuccess ? 'Delete successful' : 'Delete unsuccessful'}
        description={downloadSuccess ? 'The photo has been deleted.' : errorText}
        onButtonPress={() => {
          dispatch(setDeleteFinishedAction({ isFinished: false, isSuccess: false }))
          dispatch(NavigationActions.navigate({ routeName: routeNames.ReadyScene }))
        }}
      />
      <PopupWrapper
        isVisible={isRating}
        renderCustomContent={() => (
          <RateDialog dispatch={dispatch} />
        )}
        buttonText="Rate later"
        onButtonPress={() => dispatch(toggleRatingDialogAction())}
      />
    </View>
  )
}

export default ReviewModals
