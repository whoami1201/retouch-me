import React from 'react'
import { View, ScrollView, StyleSheet } from 'react-native'
import ExampleItem from '../ExampleItem/ExampleItem'

const ExampleScreen = ({
  categories,
}) => {
  const bodyCategory = categories ? categories[0] : null
  return (
    <View style={styles.container}>
      <ScrollView>
        {bodyCategory && bodyCategory.features.map((feature) => (
          <ExampleItem
            key={feature.slug}
            feature={feature}
          />
        ))}

      </ScrollView>
    </View>
  )
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
  },
})

export default ExampleScreen
