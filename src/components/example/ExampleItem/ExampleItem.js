import React from 'react'
import { Image, StyleSheet } from 'react-native'
import { Card } from 'react-native-elements'
import ComparisonSlider from '../../common/ComparisonSlider/ComparisonSlider'
import ThumbnailSlider from '../../../assets/icons/thumbnail-slider.png'

const THUMBNAIL_WIDTH = 64

class ExampleItem extends React.Component {
  constructor(props) {
    super(props)
    this.state = {
      imgWidth: null,
      imgHeight: null,
    }
    this.renderThumbnail = this.renderThumbnail.bind(this)
  }

  renderThumbnail = () => (
    <Image
      source={ThumbnailSlider}
      style={styles.thumbnnail}
    />
  )

  render() {
    const { feature } = this.props
    const { imgWidth, imgHeight } = this.state
    return (
      <Card
        title={feature.name}
        onLayout={(e) => {
          const { width, height } = e.nativeEvent.layout
          this.setState({ imgWidth: width, imgHeight: height })
        }}
      >
        {imgWidth && imgHeight && (
          <ComparisonSlider
            initialPosition={50}
            imageWidth={imgWidth - 30}
            imageHeight={200}
            leftImageURI={feature.example_before_img}
            rightImageURI={feature.example_after_img}
            thumnailRender={this.renderThumbnail}
            thumbnailWidth={THUMBNAIL_WIDTH}
          />
        )}
      </Card>
    )
  }
}

const styles = StyleSheet.create({
  thumbnnail: {
    width: THUMBNAIL_WIDTH,
    height: THUMBNAIL_WIDTH,
  },
})

export default ExampleItem
