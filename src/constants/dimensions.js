const defaultDimensions = {
  gutter: 16,
  largeMargin: 32,
  margin: 8,
  halfMargin: 4,
  smallMargin: 1,
}

const theme = {
  categoryBarHeight: 56,
  buttonHeight: 40,
  designBoxSizes: {
    small: 50,
    medium: 80,
    large: 110,
  },
}

export default {
  ...defaultDimensions,
  theme,
}
