const sizes = {
  h1: 24,
  h2: 20,
  h3: 16,
  h4: 12,
}

export default {
  sizes,
}
