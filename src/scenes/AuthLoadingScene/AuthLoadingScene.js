import { connect } from 'react-redux'
import React, { Component } from 'react'
import { NavigationActions } from 'react-navigation'
import { fetchClientTokenAction, fetchAccessTokenAction } from '../../redux/modules/user/user'

import LoadingMask from '../../components/common/LoadingMask/LoadingMask'
import * as routeNames from '../../constants/route-names'

class AuthLoadingScene extends Component {
  componentDidMount() {
    const { dispatch } = this.props
    dispatch(fetchClientTokenAction())
  }

  componentWillReceiveProps(nextProps) {
    const { hasUserInfoId, isLoggedIn, clientToken } = nextProps

    if (hasUserInfoId &&
      hasUserInfoId !== this.props.hasUserInfoId) {
      nextProps.dispatch(fetchAccessTokenAction())
    }
    if (isLoggedIn) {
      nextProps.dispatch(NavigationActions.navigate({ routeName: routeNames.App }))
    } else if (clientToken && !hasUserInfoId) {
      nextProps.dispatch(NavigationActions.navigate({ routeName: routeNames.SignIn }))
    }
  }

  render() {
    return <LoadingMask />
  }
}

const mapStateToProps = ({
  dispatch,
  userState,
}) => ({
  dispatch,
  hasUserInfoId: userState.info.id,
  isLoggedIn: userState.logged,
  clientToken: userState.clientToken,
})

export default connect(mapStateToProps)(AuthLoadingScene)
