import { connect } from 'react-redux'
import React from 'react'
import { View, StatusBar, StyleSheet } from 'react-native'
import { NavigationActions } from 'react-navigation'
import GalleryScreen from '../../components/gallery/GalleryScreen/GalleryScreen'
import CameraButton from '../../components/buttons/CameraButton/CameraButton'

import * as routeNames from '../../constants/route-names'

class GalleryScene extends React.PureComponent {
  static navigationOptions = ({ navigation }) => ({
    headerTitle: 'Camera Roll',
    headerLeft: <CameraButton onPress={navigation.getParam('openCamera')} />,
  })

  constructor(props) {
    super(props)
    this.openCamera = this.openCamera.bind(this)
  }

  componentDidMount() {
    this.props.navigation.setParams({ openCamera: this.openCamera })
  }

  openCamera = () => {
    this.props.dispatch(NavigationActions.navigate({ routeName: routeNames.Camera }))
  };

  render() {
    return (
      <View style={styles.container}>
        <StatusBar
          barStyle="light-content"
        />
        <GalleryScreen dispatch={this.props.dispatch} />
      </View>
    )
  }
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
  },
})

const mapStateToProps = ({
  dispatch,
}) => ({
  dispatch,
})

export default connect(mapStateToProps)(GalleryScene)
