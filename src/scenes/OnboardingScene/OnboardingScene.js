import React from 'react'
import { NavigationActions } from 'react-navigation'
import { connect } from 'react-redux'
import { StyleSheet, View, Text } from 'react-native'
import AppIntroSlider from 'react-native-app-intro-slider'
import Logo from '../../assets/icons/logo.png'
import colors from '../../constants/colors'
import dimensions from '../../constants/dimensions'
import * as routeNames from '../../constants/route-names'
import fonts from '../../constants/fonts'

const styles = StyleSheet.create({
  image: {
    width: 200,
    height: 200,
  },
  text: {
    color: colors.grey,
  },
  buttonContainer: {
    flexDirection: 'row',
    backgroundColor: colors.theme.primary,
    height: dimensions.theme.buttonHeight,
    borderRadius: dimensions.theme.buttonHeight / 2,
    justifyContent: 'center',
    alignItems: 'center',
  },
  buttonText: {
    fontSize: fonts.sizes.h3,
    color: colors.white,
    paddingLeft: dimensions.gutter,
  },
})

const slides = [
  {
    key: 'somethun',
    title: 'Title 1',
    titleStyle: styles.text,
    text: 'Description.\nSay something cool',
    image: Logo,
    imageStyle: styles.image,
    backgroundColor: colors.white,
    textStyle: styles.text,
  },
  {
    key: 'somethun-dos',
    title: 'Title 2',
    titleStyle: styles.text,
    text: 'Other cool stuff',
    textStyle: styles.text,
    image: Logo,
    imageStyle: styles.image,
    backgroundColor: colors.white,
  },
  {
    key: 'somethun1',
    title: 'Rocket guy',
    titleStyle: styles.text,
    text: 'I\'m already out of descriptions\n\nLorem ipsum bla bla bla',
    textStyle: styles.text,
    image: Logo,
    imageStyle: styles.image,
    backgroundColor: colors.white,
  },
]

class OnboardingScene extends React.Component {
  onDone = () => {
    this.props.dispatch(NavigationActions.navigate({ routeName: routeNames.Subscription }))
  }

  renderNextButton = () => (
    <View
      style={styles.buttonContainer}
    >
      <Text style={styles.buttonText}>NEXT</Text>
    </View>
  )

  renderDoneButton = () => (
    <View
      style={styles.buttonContainer}
    >
      <Text style={styles.buttonText}>DONE</Text>
    </View>
  )

  render() {
    return (
      <AppIntroSlider
        slides={slides}
        onDone={this.onDone}
        renderNextButton={this.renderNextButton}
        renderDoneButton={this.renderDoneButton}
        bottomButton
        activeDotStyle={{ backgroundColor: colors.theme.primary }}
      />
    )
  }
}

const mapStateToProps = ({
  dispatch,
}) => ({
  dispatch,
})

export default connect(mapStateToProps)(OnboardingScene)
