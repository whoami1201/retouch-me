import { connect } from 'react-redux'
import React from 'react'
import { View, StatusBar, StyleSheet } from 'react-native'
import DesignScreen from '../../components/design/DesignScreen/DesignScreen'
import { fetchCategoriesAction } from '../../redux/modules/design/design'
import UserCountIcon from '../../components/common/UserCountIcon/UserCountIcon'
import colors from '../../constants/colors'

class DesignScene extends React.PureComponent {
  static navigationOptions = {
    headerStyle: {
      backgroundColor: colors.white,
    },
    headerTintColor: colors.theme.primary,
    headerRight: (<UserCountIcon />),
  }

  componentDidMount() {
    const { designState, dispatch } = this.props
    if (designState.categories.length === 0) {
      dispatch(fetchCategoriesAction())
    }
  }

  render() {
    const { dispatch, selectedPhotoUri, designState } = this.props
    return (
      <View style={styles.container}>
        <StatusBar
          barStyle="dark-content"
        />
        <DesignScreen
          dispatch={dispatch}
          designState={designState}
          selectedPhotoUri={selectedPhotoUri}
        />
      </View>
    )
  }
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
  },
})

const mapStateToProps = ({
  dispatch,
  galleryState,
  designState,
}) => ({
  dispatch,
  designState,
  selectedPhotoUri: galleryState.selectedPhotoUri,
})

export default connect(mapStateToProps)(DesignScene)
