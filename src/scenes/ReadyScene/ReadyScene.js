import { connect } from 'react-redux'
import React from 'react'
import { View, StatusBar, StyleSheet } from 'react-native'
import { fetchPhotosAction } from '../../redux/modules/photos/photos'
import ReadyScreen from '../../components/ready/ReadyScreen/ReadyScreen'

class ReadyScene extends React.Component {
  constructor(props) {
    super(props)
    this.fetchPhotos = this.fetchPhotos.bind(this)
  }
  componentDidMount() {
    this.fetchPhotos()
  }

  fetchPhotos(page = 1) {
    this.props.dispatch(fetchPhotosAction(page))
  }

  /* eslint-disable */
  render() {
    const { photosState, dispatch, navigation } = this.props
    const {
      data,
      current_page,
      last_page,
      isLoading,
    } = photosState
    return (
      <View style={styles.container}>
        <StatusBar
          barStyle="light-content"
        />
        <ReadyScreen
          dispatch={dispatch}
          photos={data}
          isLoading={isLoading}
          onRefresh={() => this.fetchPhotos()}
          onEndReached={() => {
            if (current_page < last_page) {
              this.fetchPhotos(current_page + 1)
            }
          }}
        />
      </View>
    )
  }
  /* eslint-ensable */
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
  },
})

const mapStateToProps = ({
  dispatch,
  photosState,
}) => ({
  dispatch,
  photosState,
})

export default connect(mapStateToProps)(ReadyScene)
