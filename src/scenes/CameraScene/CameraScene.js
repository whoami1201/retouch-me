import React, { Component } from 'react'
import { connect } from 'react-redux'
import { Text, View, StyleSheet, TouchableOpacity, CameraRoll, StatusBar } from 'react-native'
import { NavigationActions } from 'react-navigation'
import { RNCamera } from 'react-native-camera'

import { setSelectedPhotoUriAction } from '../../redux/modules/gallery/gallery'
import colors from '../../constants/colors'
import * as routeNames from '../../constants/route-names'
import fonts from '../../constants/fonts'

class CameraScene extends Component {
  static navigationOptions = {
    headerStyle: {
      backgroundColor: colors.white,
    },
    headerTintColor: colors.theme.primary,
  }

  constructor(props) {
    super(props)
    this.camera = null
    this.takePicture = this.takePicture.bind(this)
  }

  takePicture = () => {
    if (this.camera) {
      const { dispatch } = this.props
      this.camera.takePictureAsync({ quality: 0.5, base64: true, skipProcessing: true })
        .then((data) => {
          CameraRoll.saveToCameraRoll(data.uri, 'photo')
          dispatch(setSelectedPhotoUriAction(data.uri))
          dispatch(NavigationActions.navigate({ routeName: routeNames.Design }))
        })
    }
  }

  render() {
    return (
      <View style={styles.container}>
        <StatusBar
          barStyle="dark-content"
        />
        <RNCamera
          ref={(ref) => { this.camera = ref }}
          style={styles.preview}
          type={RNCamera.Constants.Type.back}
          permissionDialogTitle="Permission to use camera"
          permissionDialogMessage="We need your permission to use your camera phone"
        />
        <View style={styles.captureContainer}>
          <TouchableOpacity
            onPress={this.takePicture}
            style={styles.capture}
          >
            <Text style={styles.captureText}> SNAP </Text>
          </TouchableOpacity>
        </View>
      </View>
    )
  }
}


const styles = StyleSheet.create({
  container: {
    flex: 1,
    flexDirection: 'column',
  },
  preview: {
    flex: 1,
  },
  captureText: {
    fontSize: fonts.sizes.h3,
  },
  captureContainer: {
    flex: 0,
    flexDirection: 'row',
    justifyContent: 'center',
  },
  capture: {
    flex: 0,
    backgroundColor: colors.white,
    borderRadius: 5,
    padding: 15,
    paddingHorizontal: 20,
    alignSelf: 'center',
    margin: 20,
  },
})

const mapStateToProps = ({
  dispatch,
}) => ({
  dispatch,
})

export default connect(mapStateToProps)(CameraScene)
