import { connect } from 'react-redux'
import React from 'react'
import { View, StatusBar, StyleSheet } from 'react-native'
import ReviewScreen from '../../components/ready/ReviewScreen/ReviewScreen'
import ReviewModals from '../../components/ready/ReviewModals/ReviewModals'
import colors from '../../constants/colors'

class ReviewScene extends React.Component {
  static navigationOptions = {
    headerStyle: {
      backgroundColor: colors.white,
    },
    headerTintColor: colors.theme.primary,
  }

  render() {
    const { dispatch, photosState } = this.props
    const reviewPhoto = photosState.data.find((p) => p.id === photosState.currentReviewPhotoId)
    return (
      <View style={styles.container}>
        <StatusBar
          barStyle="dark-content"
        />
        <ReviewModals
          dispatch={dispatch}
          photosState={photosState}
        />
        {reviewPhoto && (
          <ReviewScreen
            dispatch={dispatch}
            photoId={reviewPhoto.id}
            beforePhoto={reviewPhoto.img_uploaded_url}
            afterPhoto={reviewPhoto.img_processed_url}
          />
        )}
      </View>
    )
  }
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
  },
})

const mapStateToProps = ({
  dispatch,
  photosState,
}) => ({
  dispatch,
  photosState,
})

export default connect(mapStateToProps)(ReviewScene)
