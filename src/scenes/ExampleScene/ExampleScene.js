import { connect } from 'react-redux'
import React from 'react'
import ExampleScreen from '../../components/example/ExampleScreen/ExampleScreen'
import { fetchCategoriesAction } from '../../redux/modules/design/design'

class ExampleScene extends React.Component {
  componentDidMount() {
    const { dispatch, categories } = this.props
    if (categories.length === 0) {
      dispatch(fetchCategoriesAction())
    }
  }
  render() {
    const { categories } = this.props
    return (
      <ExampleScreen
        categories={categories}
      />
    )
  }
}

const mapStateToProps = ({
  dispatch,
  designState,
}) => ({
  dispatch,
  categories: designState.categories,
})

export default connect(mapStateToProps)(ExampleScene)
