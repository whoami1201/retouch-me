import { connect } from 'react-redux'
import React from 'react'
import { fetchAppConfigAction } from '../../redux/modules/meta/meta'
import SettingsScreen from '../../components/settings/SettingsScreen/SettingsScreen'

class SettingsScene extends React.Component {
  componentDidMount() {
    this.props.dispatch(fetchAppConfigAction())
  }

  render() {
    const { info, dispatch, links } = this.props

    return (
      <SettingsScreen
        info={info}
        dispatch={dispatch}
        links={links}
      />
    )
  }
}

const mapStateToProps = ({
  dispatch,
  userState,
  metaState,
}) => ({
  dispatch,
  info: userState.info,
  links: metaState.links,
})

export default connect(mapStateToProps)(SettingsScene)
