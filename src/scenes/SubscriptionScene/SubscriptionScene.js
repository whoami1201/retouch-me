import { connect } from 'react-redux'
import React from 'react'
import SubscriptionScreen from '../../components/subscription/SubscriptionScreen/SubscriptionScreen'

class SubscriptionScene extends React.Component {
  componentDidMount() {
    // TODO: fetch subscription info here
  }

  render() {
    const { subscriptionState, dispatch } = this.props

    return (
      <SubscriptionScreen
        dispatch={dispatch}
        subscriptionState={subscriptionState}
      />
    )
  }
}

const mapStateToProps = ({
  dispatch,
  subscriptionState,
}) => ({
  dispatch,
  subscriptionState,
})

export default connect(mapStateToProps)(SubscriptionScene)
