import { connect } from 'react-redux'
import React from 'react'
import { NavigationActions } from 'react-navigation'
import { SocialIcon } from 'react-native-elements'
import { Dimensions, Image, View, Text, StyleSheet } from 'react-native'
import Logo from '../../assets/icons/logo.png'
import * as routeNames from '../../constants/route-names'
import { loginFbAction, fetchAccessTokenAction, setUserLoading } from '../../redux/modules/user/user'
import { checkIfFirstLaunch } from '../../utils'
import dimensions from '../../constants/dimensions'
import LoadingMask from '../../components/common/LoadingMask/LoadingMask'
import fonts from '../../constants/fonts'

class SignInScene extends React.PureComponent {
  constructor(props) {
    super(props)
    this.state = {
      isFirstLaunch: false,
      hasCheckedAsyncStorage: false,
    }
    this.loginFacebook = this.loginFacebook.bind(this)
  }

  async componentWillMount() {
    const isFirstLaunch = await checkIfFirstLaunch()
    this.setState({ isFirstLaunch, hasCheckedAsyncStorage: true })
  }

  componentWillReceiveProps(nextProps) {
    const { userState } = this.props
    if (nextProps.userState.info.id &&
      nextProps.userState.info.id !== userState.info.id) {
      nextProps.dispatch(fetchAccessTokenAction())
    }
    if (nextProps.userState.logged) {
      nextProps.dispatch(NavigationActions.navigate({
        routeName: this.state.isFirstLaunch ? routeNames.Onboarding : routeNames.App,
      }))
    }
  }

  loginFacebook = () => {
    this.props.dispatch(setUserLoading(true))
    this.props.dispatch(loginFbAction())
  }

  render() {
    const { hasCheckedAsyncStorage } = this.state
    const { isLoading } = this.props.userState

    if (!hasCheckedAsyncStorage) {
      return null
    }

    return (
      <View style={styles.container}>
        <View style={styles.topContainer}>
          <Image
            source={Logo}
            style={styles.logo}
          />
          <Text style={styles.title}>Photo Editor</Text>
        </View>
        <View style={styles.buttonContainer}>
          {isLoading
            ? (
              <LoadingMask />
            )
            : (
              <SocialIcon
                title="Sign In With Facebook"
                button
                type="facebook"
                onPress={this.loginFacebook}
              />
            )}
        </View>
      </View>
    )
  }
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    justifyContent: 'space-around',
    alignItems: 'center',
  },
  topContainer: {
    flex: 2,
    justifyContent: 'center',
  },
  logo: {
    width: 120,
    height: 120,
    marginBottom: dimensions.gutter,
  },
  title: {
    fontSize: fonts.sizes.h1,
  },
  buttonContainer: {
    flex: 1,
    width: Dimensions.get('window').width - dimensions.largeMargin,
  },
})

const mapStateToProps = ({
  dispatch,
  userState,
}) => ({
  dispatch,
  userState,
})

export default connect(mapStateToProps)(SignInScene)
