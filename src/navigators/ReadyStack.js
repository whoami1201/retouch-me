import { createStackNavigator } from 'react-navigation'
import getSlideFromRightTransition from 'react-navigation-slide-from-right-transition'

import ReadyScene from '../scenes/ReadyScene/ReadyScene'
import ReviewScene from '../scenes/ReviewScene/ReviewScene'

import * as routeNames from '../constants/route-names'
import colors from '../constants/colors'

const ReadyStack = createStackNavigator({
  [routeNames.ReadyScene]: {
    screen: ReadyScene,
    navigationOptions: {
      title: 'Ready Photos',
      headerTintColor: colors.white,
      headerStyle: {
        backgroundColor: colors.theme.primary,
      },
    },
  },
  [routeNames.Review]: {
    screen: ReviewScene,
    navigationOptions: {
      tabBarVisible: false,
    },
  },
}, {
  transitionConfig: getSlideFromRightTransition,
  navigationOptions: {
    gesturesEnabled: true,
  },
})

ReadyStack.navigationOptions = ({ navigation }) => {
  let tabBarVisible = true
  if (navigation.state.index > 0) {
    tabBarVisible = false
  }

  return {
    tabBarVisible,
  }
}


export default ReadyStack
