import { createStackNavigator } from 'react-navigation'
import getSlideFromRightTransition from 'react-navigation-slide-from-right-transition'
import DesignScene from '../scenes/DesignScene/DesignScene'
import GalleryScene from '../scenes/GalleryScene/GalleryScene'
import CameraScene from '../scenes/CameraScene/CameraScene'
import * as routeNames from '../constants/route-names'
import colors from '../constants/colors'

const GalleryStack = createStackNavigator({
  [routeNames.GalleryScene]: {
    screen: GalleryScene,
  },
  [routeNames.Design]: {
    headerMode: 'float',
    screen: DesignScene,
  },
  [routeNames.Camera]: {
    headerMode: 'float',
    screen: CameraScene,
  },
}, {
  transitionConfig: getSlideFromRightTransition,
  navigationOptions: {
    gesturesEnabled: true,
    headerTintColor: colors.white,
    headerStyle: {
      backgroundColor: colors.theme.primary,
    },
  },
})

GalleryStack.navigationOptions = ({ navigation }) => {
  let tabBarVisible = true
  if (navigation.state.index > 0) {
    tabBarVisible = false
  }

  return {
    tabBarVisible,
  }
}

export default GalleryStack
