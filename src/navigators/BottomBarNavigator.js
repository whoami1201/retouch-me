import React from 'react'
import { createBottomTabNavigator } from 'react-navigation'
import { Image } from 'react-native'

import * as routeNames from '../constants/route-names'
import colors from '../constants/colors'

import GalleryTabIcon from '../assets/icons/tab_icon_gallery.png'
import ExampleTabIcon from '../assets/icons/tab_icon_example.png'
import ReadyTabIcon from '../assets/icons/tab_icon_ready.png'
import SettingsTabIcon from '../assets/icons/tab_icon_settings.png'

import GalleryStack from './GalleryStack'
import ReadyStack from './ReadyStack'
import SettingsStack from './SettingsStack'
import ExampleStack from './ExampleStack'
import dimensions from '../constants/dimensions'
import fonts from '../constants/fonts'

const iconStyles = {
  marginTop: dimensions.halfMargin,
  marginBottom: dimensions.halfMargin,
  height: 20,
  width: 20,
  alignSelf: 'center',
}

const getTabBarIcon = (tintColor, icon) =>
  <Image source={icon} style={[iconStyles, { tintColor }]} resizeMode="contain" />

export default createBottomTabNavigator({
  [routeNames.Gallery]: {
    screen: GalleryStack,
    navigationOptions: { tabBarIcon: ({ tintColor }) => getTabBarIcon(tintColor, GalleryTabIcon) },
  },
  [routeNames.Example]: {
    screen: ExampleStack,
    navigationOptions: { tabBarIcon: ({ tintColor }) => getTabBarIcon(tintColor, ExampleTabIcon) },
  },
  [routeNames.Ready]: {
    screen: ReadyStack,
    navigationOptions: { tabBarIcon: ({ tintColor }) => getTabBarIcon(tintColor, ReadyTabIcon) },
  },
  [routeNames.Settings]: {
    screen: SettingsStack,
    navigationOptions: { tabBarIcon: ({ tintColor }) => getTabBarIcon(tintColor, SettingsTabIcon) },
  },
}, {
  tabBarOptions: {
    scrollEnabled: true,
    activeTintColor: colors.theme.primary,
    inactiveTintColor: colors.grey,
    labelStyle: {
      fontSize: fonts.sizes.h4,
      paddingBottom: 5,
    },
    tabStyle: {
      paddingTop: dimensions.halfMargin,
      justifyContent: 'center',
      alignItems: 'center',
    },
    style: {
      backgroundColor: colors.white,
    },
    indicatorStyle: {
      backgroundColor: colors.transparent,
    },
  },
})

