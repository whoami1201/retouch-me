import { createStackNavigator } from 'react-navigation'

import SignInScene from '../scenes/SignInScene/SignInScene'

import * as routeNames from '../constants/route-names'

export default createStackNavigator({
  [routeNames.SignIn]: SignInScene,
}, {
  navigationOptions: {
    header: null,
  },
})

