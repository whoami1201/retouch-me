import { createStackNavigator } from 'react-navigation'

import BottomBarNavigator from './BottomBarNavigator'
import AuthLoadingScene from '../scenes/AuthLoadingScene/AuthLoadingScene'
import AuthStack from './AuthStack'
import OnboardingScene from '../scenes/OnboardingScene/OnboardingScene'
import SubscriptionScene from '../scenes/SubscriptionScene/SubscriptionScene'

import * as routeNames from '../constants/route-names'

const fade = (props) => {
  const { position, scene } = props

  const { index } = scene

  const translateX = 0
  const translateY = 0

  const opacity = position.interpolate({
    inputRange: [index - 0.7, index, index + 0.7],
    outputRange: [0.3, 1, 0.3],
  })

  return {
    opacity,
    transform: [{ translateX }, { translateY }],
  }
}

export default createStackNavigator(
  {
    [routeNames.AuthLoading]: AuthLoadingScene,
    [routeNames.App]: BottomBarNavigator,
    [routeNames.Auth]: AuthStack,
    [routeNames.Onboarding]: OnboardingScene,
    [routeNames.Subscription]: SubscriptionScene,
  },
  {
    initialRouteName: routeNames.initialRouteName,
    navigationOptions: {
      header: null,
      gesturesEnabled: false,
    },
    transitionConfig: () => ({
      screenInterpolator: (props) => fade(props),
    }),
  },
)
