import { createStackNavigator } from 'react-navigation'
import getSlideFromRightTransition from 'react-navigation-slide-from-right-transition'

import ExampleScene from '../scenes/ExampleScene/ExampleScene'

import * as routeNames from '../constants/route-names'
import colors from '../constants/colors'


const ExampleStack = createStackNavigator({
  [routeNames.ExampleScene]: {
    screen: ExampleScene,
    navigationOptions: {
      title: 'Examples',
      headerTintColor: colors.white,
      headerStyle: {
        backgroundColor: colors.theme.primary,
      },
    },
  },
}, {
  transitionConfig: getSlideFromRightTransition,
  navigationOptions: {
    gesturesEnabled: true,
  },
})

export default ExampleStack
