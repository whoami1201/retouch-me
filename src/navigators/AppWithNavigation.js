import _ from 'lodash'
import {
  BackHandler,
  StyleSheet,
  View,
} from 'react-native'
import { NavigationActions } from 'react-navigation'
import { connect } from 'react-redux'
import React, { Component } from 'react'

import { reduxBoundAddNavigationListener } from '../redux'
import AppNavigator from './AppNavigator'

class AppWithNavigation extends Component {
  constructor(props) {
    super(props)
    this.onBackPress = this.onBackPress.bind(this)
    // this.handleAppStateChange = this.handleAppStateChange.bind(this)
    this.navigate = this.navigate.bind(this)
  }

  componentDidMount() {
    BackHandler.addEventListener('hardwareBackPress', this.onBackPress)
    // AppState.addEventListener('change', this.handleAppStateChange)
  }

  componentWillUnmount() {
    clearTimeout(this.timeoutHandle)
    BackHandler.removeEventListener('hardwareBackPress', this.onBackPress)
    // AppState.removeEventListener('change', this.handleAppStateChange)
  }

  onBackPress() {
    const { dispatch, navigationState } = this.props
    if (navigationState.index === 0 && _.get(navigationState, 'routes[0].index') === 0) {
      return false
    }
    dispatch(NavigationActions.back())
    return true
  }

  // handleAppStateChange(nextAppState) {
  //   const { dispatch } = this.props
  //   dispatch(setAppStateAction(nextAppState))
  // }

  navigate(path) {
    const { dispatch, navigationState: { routes } } = this.props
    const currentRoute = routes[routes.length - 1].routeName
    const routeName = `${path.charAt(0).toUpperCase()}${path.slice(1)}`
    if (currentRoute !== routeName) {
      if (routeName) {
        dispatch(NavigationActions.navigate({ routeName }))
      }
    }
  }

  render() {
    const {
      dispatch,
      navigationState,
    } = this.props

    return (
      <View style={styles.container} >
        <AppNavigator
          navigation={{
            dispatch,
            state: navigationState,
            addListener: reduxBoundAddNavigationListener,
          }}
        />
      </View>
    )
  }
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
  },
})

const mapStateToProps = ({
  navigationState,
  userState,
}) => ({
  navigationState,
  userState,
})

export default connect(mapStateToProps)(AppWithNavigation)
