import { createStackNavigator } from 'react-navigation'
import getSlideFromRightTransition from 'react-navigation-slide-from-right-transition'

import SettingsScene from '../scenes/SettingsScene/SettingsScene'

import * as routeNames from '../constants/route-names'
import colors from '../constants/colors'

const SettingsStack = createStackNavigator({
  [routeNames.Settings]: {
    screen: SettingsScene,
    navigationOptions: {
      title: 'Settings',
      headerTintColor: colors.white,
      headerStyle: {
        backgroundColor: colors.theme.primary,
      },
    },
  },
}, {
  transitionConfig: getSlideFromRightTransition,
  navigationOptions: {
    gesturesEnabled: true,
  },
})

export default SettingsStack
