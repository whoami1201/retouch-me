import { Provider } from 'react-redux'
import React from 'react'
import { PersistGate } from 'redux-persist/es/integration/react'

import AppWithNavigation from './navigators/AppWithNavigation'
import configureStore from './redux'

const { persistor, store } = configureStore()

const App = () => (
  <PersistGate persistor={persistor}>
    <Provider store={store}>
      <AppWithNavigation />
    </Provider>
  </PersistGate>
)

export default App
