import { HOST, APP_BUNDLE_ID } from '../../../config'

export const apiURL = (path) => [HOST, path].join('')

const defaultHeaders = {
  'Content-Type': 'application/json',
  'X-Requested-With': 'XMLHttpRequest',
  'X-HTTP-BUNDLE': APP_BUNDLE_ID,
}

export const GET = (path, token = '') =>
  fetch(apiURL(path), {
    headers: new Headers({
      ...defaultHeaders,
      Authorization: `Bearer ${token}`,
    }),
    method: 'GET',
  })

export const POST = (path, config) =>
  fetch(apiURL(path), {
    body: JSON.stringify(config.body),
    headers: new Headers({
      ...defaultHeaders,
      ...config.headers,
    }),
    method: 'POST',
  })

export const DELETE = (path, token) =>
  fetch(apiURL(path), {
    headers: new Headers({
      ...defaultHeaders,
      Authorization: `Bearer ${token}`,
    }),
    method: 'DELETE',
  })
