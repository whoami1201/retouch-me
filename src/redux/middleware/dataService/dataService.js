import { LoginManager, AccessToken, GraphRequest, GraphRequestManager } from 'react-native-fbsdk'
import { Platform, CameraRoll } from 'react-native'
import RNFetchBlob from 'rn-fetch-blob'
import { GET, POST, DELETE } from './api'
import { updateCategoriesAction, receiveUploadResultAction } from '../../modules/design/design'
import { CLIENT_SECRET, CLIENT_ID, API_PREFIX, API_VERSION } from '../../../config'
import {
  setUserLoading,
  receiveAccessTokenAction,
  receiveClientTokenAction,
  receiveFbInfoAction,
  receiveAccessCodeAction,
} from '../../modules/user/user'
import { receiveAppConfigAction } from '../../../redux/modules/meta/meta'
import { uploadImageCloudinary, CLOUD_NAME } from '../../../utils'
import {
  receivePhotosAction,
  setDeleteFinishedAction,
  setPhotosLoading,
  setDownloadFinishedAction,
} from '../../modules/photos/photos'

const FETCH_CATEGORY_LIST = 'api/FETCH_CATEGORY_LIST'
const FETCH_CLIENT_TOKEN = 'auth/FETCH_CLIENT_TOKEN'
const FETCH_ACCESS_TOKEN = 'auth/FETCH_ACCESS_TOKEN'
const LOGIN_FACEBOOK = 'auth/LOGIN_FACEBOOK'
const UPLOAD_PHOTO = 'api/UPLOAD_PHOTO'
const FETCH_APP_CONFIG = 'api/FETCH_APP_CONFIG'
const FETCH_PHOTOS = 'api/FETCH_PHOTOS'
const DOWNLOAD_PHOTO = 'api/DOWNLOAD_PHOTO'
const DELETE_PHOTO = 'api/DELETE_PHOTO'

const PER_PAGE = 10

export const fetchCategoryList = () => ({
  type: FETCH_CATEGORY_LIST,
})

export const fetchClientToken = () => ({
  type: FETCH_CLIENT_TOKEN,
})

export const loginFb = () => ({
  type: LOGIN_FACEBOOK,
})

export const fetchAccessToken = () => ({
  type: FETCH_ACCESS_TOKEN,
})

export const uploadPhoto = (uri) => ({
  type: UPLOAD_PHOTO,
  uri,
})

export const fetchAppConfig = () => ({
  type: FETCH_APP_CONFIG,
})

export const fetchPhotos = (page) => ({
  type: FETCH_PHOTOS,
  page,
})

export const downloadPhoto = (uri) => ({
  type: DOWNLOAD_PHOTO,
  uri,
})

export const deletePhoto = (photoId) => ({
  type: DELETE_PHOTO,
  photoId,
})

const handleLoadError = (err) => {
  throw new Error(err)
}

const headerWithToken = (token) => ({
  Authorization: `Bearer ${token}`,
})

const dataService = (store) => (next) => (action) => {
  let userState
  let designState
  let sections

  next(action)

  switch (action.type) {
    case FETCH_CATEGORY_LIST:
      userState = store.getState().userState // eslint-disable-line
      return GET(`/api${API_VERSION}/photoeditor/categories`, userState.accessToken)
        .then((res) => res.json())
        .then((categories) => {
          next(updateCategoriesAction(categories))
        })
        .catch(handleLoadError)

    case FETCH_CLIENT_TOKEN:
      return POST('/oauth/token', {
        body: {
          grant_type: 'client_credentials',
          client_id: CLIENT_ID,
          client_secret: CLIENT_SECRET,
        },
      })
        .then((response) => response.json())
        .then((data) => next(receiveClientTokenAction(data.access_token)))

    case LOGIN_FACEBOOK:
      return LoginManager
        .logInWithReadPermissions(['public_profile', 'user_birthday', 'user_gender', 'email'])
        .then((result) => {
          if (result.isCancelled) {
            next(setUserLoading(false))
            alert('Login cancelled')
          } else {
            AccessToken
              .getCurrentAccessToken()
              .then((value) => {
                next(receiveAccessCodeAction(value.accessToken))
                new GraphRequestManager()
                  .addRequest(new GraphRequest(
                    '/me',
                    {
                      parameters: {
                        fields: {
                          string: 'name,email,gender,birthday,picture',
                        },
                        access_token: {
                          string: value.accessToken,
                        },
                      },
                    },
                    (error, graphData) => {
                      if (error) {
                        next(setUserLoading(false))
                        alert(`Error fetching data: ${error.toString()}`)
                      } else {
                        next(receiveFbInfoAction(graphData))
                      }
                    },
                  ))
                  .start()
              })
              .catch((err) => alert(`Login error: ${err}`))
          }
        })

    case FETCH_ACCESS_TOKEN:
      userState = store.getState().userState // eslint-disable-line
      return POST('/api/auth/login/facebook', {
        headers: headerWithToken(userState.clientToken),
        body: {
          access_code: userState.accessCode,
          type: 'user',
          device_token: 'ios_iphonex2_device_token',
          device_id: 'ios_iphonex2_device_id',
          platform: Platform.OS,
          gender: userState.info.gender,
          meta: {
            birthday: userState.info.birthday,
          },
        },
      })
        .then((res) => res.json())
        .then((data) => {
          next(receiveAccessTokenAction({
            accessToken: data.access_token,
            refreshToken: data.refresh_token,
          }))
        })
        .catch((err) => {
          next(setUserLoading(false))
          alert(`Login error: ${err}`)
        })

    case UPLOAD_PHOTO:
      userState = store.getState().userState // eslint-disable-line
      designState = store.getState().designState // eslint-disable-line
      sections = designState.models.map((model) => ({
        id: model.id,
        location: model.location,
        selected_features: model.selectedFeatures,
        box_size: model.boxSize,
        canvas_width: designState.canvasWidth,
        canvas_height: designState.canvasHeight,
      }))
      return uploadImageCloudinary(action.uri)
        .then((cloudinaryRessult) => {
          POST(`${API_PREFIX}${API_VERSION}/photoeditor/photos`, {
            headers: headerWithToken(userState.accessToken),
            body: {
              img_uploaded_url: cloudinaryRessult.url,
              cloudinary_username: CLOUD_NAME,
              cloudinary_public_id: cloudinaryRessult.public_id,
              sections,
            },
          }).then((res) => res.json())
            .then((result) => {
              next(receiveUploadResultAction(result))
            })
        })

    case FETCH_APP_CONFIG:
      return GET('/api/configs/photoeditor')
        .then((res) => res.json())
        .then((config) => {
          next(receiveAppConfigAction(config))
        })
        .catch(handleLoadError)

    case FETCH_PHOTOS:
      next(setPhotosLoading(true))
      userState = store.getState().userState // eslint-disable-line
      return GET(`/api${API_VERSION}/photoeditor/photos?page[number]=${action.page}&page[size]=${PER_PAGE}`,
        userState.accessToken)
        .then((res) => res.json())
        .then((photos) => {
          next(receivePhotosAction(photos))
        })
        .catch(handleLoadError)

    case DOWNLOAD_PHOTO:
      return RNFetchBlob
        .config({
          fileCache: true,
          appendExt: 'jpg',
        })
        .fetch('GET', action.uri)
        .then((result) => {
          if (result.respInfo.status === 200) {
            const path = result.path()
            CameraRoll.saveToCameraRoll(
              Platform.OS === 'android' ? `file://${path}` : path,
              'photo',
            ).then(() => {
              next(setDownloadFinishedAction({
                isFinished: true, isSuccess: true,
              }))
            })
          } else {
            next(setDownloadFinishedAction({
              isFinished: true, isSuccess: false,
            }))
          }
        })

    case DELETE_PHOTO:
      userState = store.getState().userState // eslint-disable-line
      return DELETE(`/api${API_VERSION}/photoeditor/photos/${action.photoId}`, userState.accessToken)
        .then((res) => {
          if (res.status === 204) {
            next(setDeleteFinishedAction({
              isFinished: true,
              isSuccess: true,
              photoId: action.photoId,
            }))
          } else {
            next(setDeleteFinishedAction({ isFinished: true, isSuccess: false }))
          }
        })

    default:
      break
  }

  return null
}

export default dataService
