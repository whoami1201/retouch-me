import { persistCombineReducers } from 'redux-persist'
import storage from 'redux-persist/lib/storage'

import navigationReducer from './modules/navigation/navigation'
import designReducer from './modules/design/design'
import galleryReducer from './modules/gallery/gallery'
import userReducer from './modules/user/user'
import metaReducer from './modules/meta/meta'
import photosReducer from './modules/photos/photos'

export default persistCombineReducers({
  key: 'root',
  storage,
  whitelist: [
    'navigationState',
    'metaState',
    'userState',
  ],
}, {
  designState: designReducer,
  navigationState: navigationReducer,
  galleryState: galleryReducer,
  userState: userReducer,
  metaState: metaReducer,
  photosState: photosReducer,
})
