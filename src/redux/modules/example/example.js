// Actions
const SET_SELECTED_PHOTO_URI = 'gallery/SET_SELECTED_PHOTO_URI'

// Reducer
const defaultState = {
  selectedPhotoUri: null,
}

export default function reducer(state = defaultState, action) {
  switch (action.type) {
    case SET_SELECTED_PHOTO_URI:
      return {
        ...state,
        selectedPhotoUri: action.uri,
      }

    default:
      return state
  }
}

// Action creators
export const setSelectedPhotoUriAction = (uri) => ({
  type: SET_SELECTED_PHOTO_URI,
  uri,
})
