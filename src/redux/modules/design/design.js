import _ from 'lodash'
import { fetchCategoryList, uploadPhoto } from '../../middleware/dataService/dataService'
import dimensions from '../../../constants/dimensions'

export const getBoxDimension = (boxSize) => (
  dimensions.theme.designBoxSizes[boxSize]
)

export const BOX_SIZES = {
  SMALL: 'small',
  MEDIUM: 'medium',
  LARGE: 'large',
}

export const DEFAULT_BOX_SIZE = BOX_SIZES.MEDIUM

// Actions
const UPDATE_NOTIFICATION_VISIBLE = 'design/UPDATE_NOTIFICATION_VISIBLE'
const UPDATE_CATEGORIES = 'design/UPDATE_CATEGORIES'
const ADD_NEW_MODEL = 'design/ADD_NEW_MODEL'
const REMOVE_MODEL = 'design/REMOVE_MODEL'
const UPDATE_CURRENT_MODEL_ID = 'design/UPDATE_CURRENT_MODEL_ID'
const UPDATE_CURRENT_CATEGORY_ID = 'design/UPDATE_CURRENT_CATEGORY_ID'
const ADD_SELECTED_FEATURE = 'design/ADD_SELECTED_FEATURE'
const REMOVE_SELECTED_FEATURE = 'design/REMOVE_SELECTED_FEATURE'
const RESET_DESIGN_STATE = 'design/RESET_DESIGN_STATE'
const RECEIVE_UPLOAD_RESULT = 'design/RECEIVE_UPLOAD_RESULT'
const UPDATE_CANVAS_SIZE = 'design/UPDATE_CANVAS_SIZE'

// Reducer
const defaultState = {
  isNotificationVisible: true,
  categories: [],
  currentModelId: null,
  models: [],
  uploadResult: null,
  canvasWidth: null,
  canvasHeight: null,
}

export default function reducer(state = defaultState, action) {
  let model
  let id

  switch (action.type) {
    case UPDATE_NOTIFICATION_VISIBLE:
      return {
        ...state,
        isNotificationVisible: action.value,
      }

    case UPDATE_CATEGORIES:
      return {
        ...state,
        categories: action.categories,
      }

    case ADD_NEW_MODEL:
      if (state.models.length === 2) {
        return {
          ...state,
        }
      }
      id = state.models.length > 0
        ? _.maxBy(state.models, (m) => m.id).id + 1
        : 1
      model = {
        id,
        location: action.location,
        boxSize: getBoxDimension(DEFAULT_BOX_SIZE),
        selectedFeatures: [],
      }
      return {
        ...state,
        currentModelId: model.id,
        models: [...state.models, model],
      }

    case REMOVE_MODEL:
      return {
        ...state,
        models: state.models
          .filter((m) => m.id !== action.modelId)
          .map((m) => ((m.id > action.modelId)
            ? { ...m, id: m.id - 1 }
            : m
          )),
        currentModelId: 0,
      }

    case UPDATE_CURRENT_MODEL_ID:
      return {
        ...state,
        currentModelId: action.modelId,
      }

    case UPDATE_CURRENT_CATEGORY_ID:
      return {
        ...state,
        currentCategoryId: action.categoryId,
      }

    case ADD_SELECTED_FEATURE:
      return {
        ...state,
        models: state.models.map((m) => {
          if (m.id === state.currentModelId) {
            return {
              ...m,
              selectedFeatures: [...m.selectedFeatures, action.featureId],
            }
          }
          return m
        }),
      }

    case REMOVE_SELECTED_FEATURE:
      return {
        ...state,
        models: state.models.map((m) => {
          if (m.id === state.currentModelId) {
            return {
              ...m,
              selectedFeatures: _.pull(m.selectedFeatures, action.featureId),
            }
          }
          return m
        }),
      }

    case RESET_DESIGN_STATE:
      return {
        ...defaultState,
        isNotificationVisible: false,
        categories: state.categories,
      }

    case RECEIVE_UPLOAD_RESULT:
      return {
        ...state,
        uploadResult: action.result,
      }

    case UPDATE_CANVAS_SIZE:
      return {
        ...state,
        canvasWidth: action.width,
        canvasHeight: action.height,
      }

    default:
      return state
  }
}

// Action creators
export const updateNotificationVisibleAction = (value) => ({
  type: UPDATE_NOTIFICATION_VISIBLE,
  value,
})

export const updateCategoriesAction = (categories) => ({
  type: UPDATE_CATEGORIES,
  categories,
})

export const addNewModelAction = (location) => ({
  type: ADD_NEW_MODEL,
  location,
})

export const removeModelAction = (modelId) => ({
  type: REMOVE_MODEL,
  modelId,
})

export const updateCurrentModelIdAction = (modelId) => ({
  type: UPDATE_CURRENT_MODEL_ID,
  modelId,
})

export const updateCurrentCategoryId = (categoryId) => ({
  type: UPDATE_CURRENT_CATEGORY_ID,
  categoryId,
})

export const addSelectedFeature = (featureId) => ({
  type: ADD_SELECTED_FEATURE,
  featureId,
})

export const removeSelectedFeature = (featureId) => ({
  type: REMOVE_SELECTED_FEATURE,
  featureId,
})

export const resetDesignState = () => ({
  type: RESET_DESIGN_STATE,
})

export const receiveUploadResultAction = (result) => ({
  type: RECEIVE_UPLOAD_RESULT,
  result,
})

export const updateCanvasSize = ({ width, height }) => ({
  type: UPDATE_CANVAS_SIZE,
  width,
  height,
})

export const fetchCategoriesAction = () => (
  (dispatch) => dispatch(fetchCategoryList())
)

export const uploadPhotoAction = (uri) => (
  (dispatch) => dispatch(uploadPhoto(uri))
)
