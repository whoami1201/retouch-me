import { fetchClientToken, loginFb, fetchAccessToken } from '../../middleware/dataService/dataService'

// Actions
const RECEIVE_CLIENT_TOKEN = 'user/RECEIVE_CLIENT_TOKEN'
const RECEIVE_FB_INFO = 'user/RECEIVE_FB_INFO'
const RECEIVE_ACCESS_CODE = 'user/RECEIVE_ACCESS_CODE'
const RECEIVE_ACCESS_TOKEN = 'user/RECEIVE_ACCESS_TOKEN'
const SET_USER_LOADING = 'user/SET_USER_LOADING'

// Reducer
const defaultState = {
  isLoading: false,
  logged: false,
  clientToken: '',
  accessToken: '',
  refreshToken: '',
  accessCode: '',
  info: {},
}

export default function reducer(state = defaultState, action) {
  switch (action.type) {
    case RECEIVE_CLIENT_TOKEN:
      return {
        ...state,
        clientToken: action.token,
      }

    case RECEIVE_ACCESS_CODE:
      return {
        ...state,
        accessCode: action.accessCode,
      }

    case RECEIVE_FB_INFO:
      return {
        ...state,
        info: action.info,
      }

    case RECEIVE_ACCESS_TOKEN:
      return {
        ...state,
        logged: true,
        accessToken: action.accessToken,
        refreshToken: action.refreshToken,
      }

    case SET_USER_LOADING:
      return {
        ...state,
        isLoading: action.isLoading,
      }

    default:
      return state
  }
}

// Action creators
export const receiveClientTokenAction = (token) => ({
  type: RECEIVE_CLIENT_TOKEN,
  token,
})

export const receiveFbInfoAction = (info) => ({
  type: RECEIVE_FB_INFO,
  info,
})

export const receiveAccessCodeAction = (accessCode) => ({
  type: RECEIVE_ACCESS_CODE,
  accessCode,
})

export const receiveAccessTokenAction = ({ accessToken, refreshToken }) => ({
  type: RECEIVE_ACCESS_TOKEN,
  accessToken,
  refreshToken,
})

export const setUserLoading = (isLoading) => ({
  type: SET_USER_LOADING,
  isLoading,
})

export const fetchClientTokenAction = () => (
  (dispatch) => dispatch(fetchClientToken())
)

export const loginFbAction = () => (
  (dispatch) => dispatch(loginFb())
)

export const fetchAccessTokenAction = () => (
  (dispatch) => dispatch(fetchAccessToken())
)
