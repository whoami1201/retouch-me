import { uniqBy, without, flatten, sortBy } from 'lodash'
import { fetchPhotos, downloadPhoto, deletePhoto } from '../../middleware/dataService/dataService'

// Actions
const RECEIVE_PHOTOS = 'photos/RECEIVE_PHOTOS'
const SET_PHOTOS_LOADING = 'photos/SET_PHOTOS_LOADING'
const SET_CURRENT_REVIEW_PHOTO = 'photos/SET_CURRENT_REVIEW_PHOTO'
const SET_DOWNLOAD_FINISHED = 'photos/SET_DOWNLOAD_FINISHED'
const SET_DELETE_FINISHED = 'photos/SET_DELETE_FINISHED'
const TOGGLE_RATING_DIALOG = 'photos/TOGGLE_RATING_DIALOG'

// Reducer
const defaultState = {
  data: [],
  isLoading: false,
  isRating: false,
  downloadFinished: false,
  downloadSuccess: false,
  deleteFinished: false,
  deleteSuccess: false,
  currentReviewPhotoId: null,
}

export default function reducer(state = defaultState, action) {
  switch (action.type) {
    case RECEIVE_PHOTOS:
      return {
        ...state,
        ...action.result,
        data: uniqBy(sortBy(without(flatten([...state.data, action.result.data]), undefined), 'processed_at'), 'id'),
        isLoading: false,
      }

    case SET_PHOTOS_LOADING:
      return {
        ...state,
        isLoading: action.isLoading,
      }

    case SET_CURRENT_REVIEW_PHOTO:
      return {
        ...state,
        currentReviewPhotoId: action.id,
      }

    case SET_DOWNLOAD_FINISHED:
      return {
        ...state,
        downloadFinished: action.isFinished,
        downloadSuccess: action.isSuccess,
      }

    case SET_DELETE_FINISHED:
      return {
        ...state,
        data: action.photoId
          ? state.data.filter((photo) => photo.id !== action.photoId)
          : state.data,
        currentReviewPhotoId: null,
        deleteFinished: action.isFinished,
        deleteSuccess: action.isSuccess,
      }

    case TOGGLE_RATING_DIALOG:
      return {
        ...state,
        isRating: !state.isRating,
      }

    default:
      return state
  }
}

// Action creators
export const receivePhotosAction = (result) => ({
  type: RECEIVE_PHOTOS,
  result,
})

export const setPhotosLoading = (isLoading) => ({
  type: SET_PHOTOS_LOADING,
  isLoading,
})

export const setCurrentViewPhoto = (id) => ({
  type: SET_CURRENT_REVIEW_PHOTO,
  id,
})

export const setDownloadFinishedAction = ({ isFinished, isSuccess }) => ({
  type: SET_DOWNLOAD_FINISHED,
  isFinished,
  isSuccess,
})

export const setDeleteFinishedAction = ({ isFinished, isSuccess, photoId }) => ({
  type: SET_DELETE_FINISHED,
  isFinished,
  isSuccess,
  photoId,
})

export const toggleRatingDialogAction = () => ({
  type: TOGGLE_RATING_DIALOG,
})

export const downloadPhotoAction = (uri) => (
  (dispatch) => dispatch(downloadPhoto(uri))
)

export const deletePhotoAction = (photoId) => (
  (dispatch) => dispatch(deletePhoto(photoId))
)

export const fetchPhotosAction = (page) => (
  (dispatch) => dispatch(fetchPhotos(page))
)
