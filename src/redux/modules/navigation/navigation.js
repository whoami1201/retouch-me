import AppNavigator from '../../../navigators/AppNavigator'
import { initialRouteName } from '../../../constants/route-names'

const initialState = AppNavigator.router
  .getStateForAction(AppNavigator.router.getActionForPathAndParams(initialRouteName))

export default function reducer(state = initialState, action) {
  const nextState = AppNavigator.router.getStateForAction(action, state)
  return nextState ? { ...state, ...nextState } : state
}

