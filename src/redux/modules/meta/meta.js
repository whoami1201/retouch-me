import { fetchAppConfig } from '../../middleware/dataService/dataService'

// Actions
const RECEIVE_APP_CONFIG = 'meta/RECEIVE_APP_CONFIG'

// Reducer
const defaultState = {
  links: {},
}

export default function reducer(state = defaultState, action) {
  switch (action.type) {
    case RECEIVE_APP_CONFIG:
      return {
        ...state,
        links: action.config.links,
      }

    default:
      return state
  }
}

// Action creators
export const receiveAppConfigAction = (config) => ({
  type: RECEIVE_APP_CONFIG,
  config,
})

export const fetchAppConfigAction = () => (
  (dispatch) => dispatch(fetchAppConfig())
)
