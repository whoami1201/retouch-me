import { createStore, applyMiddleware, compose } from 'redux'
import thunk from 'redux-thunk'
import {
  createReactNavigationReduxMiddleware,
  createReduxBoundAddListener,
} from 'react-navigation-redux-helpers'
import { persistStore } from 'redux-persist'
import devTools from 'remote-redux-devtools'
import rootReducer from './reducers'
import dataService from './middleware/dataService/dataService'

const navigationMiddleware = createReactNavigationReduxMiddleware(
  'root',
  (state) => state.navigation,
)

export const reduxBoundAddNavigationListener = createReduxBoundAddListener('root')

export default function () {
  const enhancer = compose(
    applyMiddleware(thunk, dataService, navigationMiddleware),
    devTools({
      hostname: 'localhost',
      port: 8000,
    }),
  )
  const store = {
    ...createStore(rootReducer, enhancer),
  }
  const persistor = persistStore(store)
  return {
    persistor,
    store,
  }
}
